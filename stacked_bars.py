import sys
import pickle
import matplotlib.pyplot as plt
import numpy as np
import glob

data = []
labels = []
fig = plt.figure()
ax  = fig.add_subplot(111)

results = [
"/home/nseydoux/dev/edr/results/coopis/iter_11/clone_f_0_reproduce/results_clone_f_0_1",
"/home/nseydoux/dev/edr/results/coopis/iter_11/clone_f_1_reproduce/results_clone_f_1_1",
"/home/nseydoux/dev/edr/results/coopis/iter_11/clone_f_2_reproduce/results_clone_f_2_1"
# "/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_0_rpi23sb",
# "/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_1_rpi23sb",
# "/home/nseydoux/dev/edr/results/coopis/iter_12/scala/results_clone_f_2_rpi23sb"
]

index = 0

labels = []
transit = []
processing = []
idle = []

for path in results:
    index += 1
    for method in ["cip", "cdp", "cdr", "cir", "adp"]:
        print(method)
    # for method in ["adp", "cir"]:

        journeys = []
        with open(path+"/journeys_{0}.pkl".format(method), "rb") as f:
            journeys = pickle.load(f)

        method_transit = [j["transit"] for j in journeys] # if j["transit"] < 7000]
        method_idle = [j["idle"] for j in journeys] # if j["idle"] < 7000]
        method_processing = [j["processing"] for j in journeys] # if j["processing"] < 7000]

        width = 0.35       # the width of the bars: can also be len(x) sequence

        labels.append("s"+str(index)+"\n"+method)

        # normalization
        avg_t = np.average(method_transit)
        avg_p = np.average(method_processing)
        avg_i = np.average(method_idle)
        avg_d = avg_t+avg_p+avg_i

        transit.append(avg_t*100.0/avg_d)
        processing.append(avg_p*100.0/avg_d)
        idle.append(avg_i*100.0/avg_d)

p1 = plt.bar(labels, transit, width, color="0.9", hatch="//", linewidth=1, edgecolor="black")
p2 = plt.bar(labels, processing, width, color="0.5", hatch="\\", bottom=transit, linewidth=1, edgecolor="black")
p3 = plt.bar(labels, idle, width, bottom=[x + y for x, y in zip(transit, processing)], color="0.7", hatch="x", linewidth=1, edgecolor="black")

plt.ylabel('Durations (%)')
plt.xlabel('Topologies')
plt.title('Normalized repartition of delays')
# plt.yticks(np.arange(0, 81, 10))
plt.legend((p1[0], p2[0], p3[0]), ('Transit', 'Processing', 'Idle'), loc = 'lower right')
plt.show()
