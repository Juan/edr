package fr.irit.melodi.edr.in.resources;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.charset.StandardCharsets;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.node.Node;

@Path("/data")
public class DataEndpoint {
	private static final Logger LOGGER = LogManager.getLogger(DataEndpoint.class);

	@POST
	@Path("/enriched")
	@Consumes("text/turtle")
	public Response pushEnrichedData(String data){
		try{
			Model m = ModelFactory.createDefaultModel();
			InputStream stream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));
			m.read(stream, "http://example.com/ns#", "TTL");
			Node.getInstance().processNewData(m);
			return  Response.status(HttpURLConnection.HTTP_OK).build();
		} catch(Exception e){
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_SERVER_ERROR).build();
		}
		
	}
	
	@POST
	@Path("/raw")
	@Consumes("text/csv")
	public Response pushRawData(String data){
		try{
			LOGGER.debug("Received raw data : "+data);
			Node.getInstance().processNewRawData(data);
			return  Response.status(HttpURLConnection.HTTP_OK).build();
		} catch(Exception e){
			e.printStackTrace();
			return Response.status(HttpURLConnection.HTTP_SERVER_ERROR).build();
		}
		
	}
}
