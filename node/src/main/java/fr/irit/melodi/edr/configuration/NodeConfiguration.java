package fr.irit.melodi.edr.configuration;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class NodeConfiguration {
	private int id;
	private String name;
	private String host;
	private Integer port;
	private List<String> tbox;
	private String queries;
	private List<String> remotes;
	private Set<Map<String, String>> sensors;
	private Boolean local;
	private Boolean reasoning;
	private E_Centralization centralization;
	private String feature;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public List<String> getTbox() {
		return tbox;
	}

	public void setTbox(List<String> tbox) {
		this.tbox = tbox;
	}

	public String getQueries() {
		return queries;
	}

	public void setQueries(String queries) {
		this.queries = queries;
	}

	public List<String> getRemotes() {
		return remotes;
	}

	public void setRemotes(List<String> remotes) {
		this.remotes = remotes;
	}

	public Boolean getLocal() {
		return local;
	}

	public void setLocal(Boolean local) {
		this.local = local;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getReasoning() {
		return reasoning;
	}

	public void setReasoning(Boolean reasoning) {
		this.reasoning = reasoning;
	}

	public Set<Map<String, String>> getSensors() {
		return sensors;
	}

	public void setSensors(Set<Map<String, String>> sensors) {
		this.sensors = sensors;
	}

	public E_Centralization getCentralization() {
		return centralization;
	}

	public void setCentralization(String centralization) {
		this.centralization = E_Centralization.fromString(centralization);
	}

	public String getFeature() {
		return feature;
	}

	public void setFeature(String feature) {
		this.feature = feature;
	}

}
