package fr.irit.melodi.edr.out.resources;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;

public class RulePropagationClient {
private static final Logger LOGGER = LogManager.getLogger(RulePropagationClient.class);
	

	public static void sendRule(String uri, String rule){
		LOGGER.debug("Sending rule to endpoint "+uri);
		Client client = ClientBuilder.newClient(new ClientConfig());
		Response r = client.target(uri)
	        .path("/rules/add")
	        .request()//"application/rdf+ttl"
	        .post(Entity.entity(rule, "text/turtle"));
		if(r.getStatus() == 200){
			LOGGER.debug("Rule successfully transmitted");
		} else {
			LOGGER.error("Annouce failed with status code "+r.getStatus());
		}
	}
}
