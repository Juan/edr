package fr.irit.melodi.edr.in.resources;

import java.net.HttpURLConnection;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import fr.irit.melodi.edr.node.Node;

@Path("/model")
public class ModelEndpoint {

	@POST
	@Path("/load")
	public Response loadModel(String uri){
		Node.getInstance().getKBManager().loadModel(uri);
		return  Response.status(HttpURLConnection.HTTP_OK)
                .entity("Model loaded").build();
	}
	
	@GET
	@Path("/all")
	public Response getKB(){
		return Response
				.status(HttpURLConnection.HTTP_OK)
				.entity(Node.getInstance().getKBManager().serializeModel())
				.build();
	}
}
