package fr.irit.melodi.edr.out.client;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class HTTPClient {
	private CloseableHttpClient httpclient;
	private CredentialsProvider credentials;
	
	// opa-enricher
	private String authenticationHeader = "Basic b3BhLWVucmljaGVyOk9QQS1lbnJpY2htZW50LTRsaWZl";
	
	public HTTPClient(String server, int port, String username, String password) {
		this.credentials = new BasicCredentialsProvider();
		credentials.setCredentials(
		    new AuthScope(server, port), 
		    new UsernamePasswordCredentials(username, password));
	}
	
	public void connect() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException{
		SSLContextBuilder builder = new SSLContextBuilder();
	    builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
	            builder.build());
	    this.httpclient = HttpClients.custom()
	    		.setSSLSocketFactory(sslsf)
	    		.setDefaultCredentialsProvider(this.credentials)
	    		.build();
	}
	
	public String get(String url) throws ClientProtocolException, IOException{
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Authorization", this.authenticationHeader);
		CloseableHttpResponse response = httpclient.execute(httpGet);
		HttpEntity entity = null;
		entity = response.getEntity();
//	    try {
//	        entity = response.getEntity();
//	    }
//	    finally {
//	        response.close();
//	    }
	    return EntityUtils.toString(entity);
	}
	
	public StatusLine post(String url, String body) throws ClientProtocolException, IOException{
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Authorization", this.authenticationHeader);
		httpPost.setEntity(new StringEntity(body));
		CloseableHttpResponse response = httpclient.execute(httpPost);
		HttpEntity entity = null;
	    try {
	        entity = response.getEntity();
	    }
	    finally {
	        response.close();
	    }
	    return response.getStatusLine();
	}
}