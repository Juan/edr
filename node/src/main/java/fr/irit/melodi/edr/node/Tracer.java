package fr.irit.melodi.edr.node;

import java.text.MessageFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Tracer {
	private static final Logger TRACEFILE = LogManager.getLogger("TraceFile");

	public static void logTrace(String trace) {
		TRACEFILE.info("\"node\":\"" + Node.getInstance().getUri() + "\"," +"\"host\":\""+Node.getInstance().getConfiguration().getHost()+"\","+ trace);
	}

	public static void reasoningStarts(String newObservation, String rule) {
		logTrace(MessageFormat.format("\"event\":\"Reasoning starts\",\"observation\":\"{0}\", \"rule\":\"{1}\"",
				newObservation, rule));
	}
	
	public static void reasoningStarts(String newObservation) {
		logTrace(MessageFormat.format("\"event\":\"Reasoning starts\",\"observation\":\"{0}\"",
				newObservation));
	}

	public static void reasoningEnds(String newObservation, String rule, Boolean deduction) {
		logTrace(MessageFormat.format(
				"\"event\":\"Reasoning ends\",\"observation\":\"{0}\", \"rule\":\"{1}\", \"deduction\":\"{2}\"",
				newObservation, rule, (deduction ? "true" : "false")));
	}
	
	public static void reasoningEnds(String newObservation, Boolean deduction) {
		logTrace(MessageFormat.format(
				"\"event\":\"Reasoning ends\",\"observation\":\"{0}\", \"deduction\":\"{1}\"",
				newObservation, (deduction ? "true" : "false")));
	}

	public static void observationSent(String observation, String target) {
		logTrace(MessageFormat.format("\"event\":\"Sending data\",\"observation\":\"{0}\", \"target\":\"{1}\"",
				observation, target));
	}

	public static void observationReceived(String observation) {
		logTrace(MessageFormat.format("\"event\":\"Received observation\",\"observation\":\"{0}\"", observation));
	}

	public static void enrichmentStarts() {
		logTrace("\"event\":\"Enrichment starts\"");
	}

	public static void enrichmentEnded(String obs) {
		logTrace("\"event\":\"Enrichment ended\", \"observation\":\"" + obs + "\"");
	}
}
