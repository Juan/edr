package fr.irit.melodi.edr.model;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.Marker;
import org.topbraid.shacl.util.ModelPrinter;

import fr.irit.melodi.edr.configuration.E_Centralization;
import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.edr.out.resources.RulePropagationClient;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.melodi.sparql.query.select.SparqlSelect;
import fr.irit.tools.queries.QueryEngine;

public class RuleManager {
	private static final Logger LOGGER = LogManager.getLogger(RuleManager.class);
	private static final Marker MODEL_MARKER = MarkerManager.getMarker("MODEL_MARKER");
	
	private static final Logger LOGMODEL = LogManager.getLogger("LogModel");
	public static final String SHACL_URI = "http://www.w3.org/ns/shacl#";
	private FolderManager queries;
	public static Set<Entry<String, String>> prefixes;

	static {
		prefixes = new HashSet<Entry<String, String>>();
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("sh", "<http://www.w3.org/ns/shacl#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("ex", "<http://example.com/ns#>"));
	}

	public RuleManager() {
		try {
			this.queries = new FolderManager("queries/rules");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}

	public void updateRule(Rule r) throws UnimplementedFeatureException {
		if (E_Centralization.rulePropagationAllowed(Node.getInstance().getConfiguration().getCentralization())) {
			Boolean aggregates = this.isAggregator(r);
			if (aggregates != null) {
				LOGGER.info(Node.getInstance().getName() + " is " + (aggregates ? "" : "not") + " aggregator for "
						+ r.getUri());
				this.markRuleActivity(r.getUri(), aggregates);
				if (aggregates) {
					Node.getInstance().getInterestManager().propagateInterests();
				}
			} else {
				LOGGER.warn("The aggregator query returned null for node " + Node.getInstance().getName() + " and rule "
						+ r.getUri());

			}
			if (this.canApplyRule(r)) {
				LOGGER.debug(Node.getInstance().getName() + " can apply rule " + r.getUri());
				this.markRuleActivity(r.getUri(), true);
			} else {
				LOGGER.debug(Node.getInstance().getName() + " cannot apply rule " + r.getUri());
				this.markRuleActivity(r.getUri(), false || aggregates);
			}
			Set<String> transferrableTo = this.ruleTransferrableTo(r);
			LOGGER.info("Rule " + r.getUri() + " is transmitted to " + transferrableTo.size() + " sensors");
			for (String remote : transferrableTo) {
				this.transferRule(r, remote);
			}
		} else {
			// In the centralized case, this piece of code will only run on
			// nodes directly being assigned a rule
			this.markRuleActivity(r.getUri(), true);
			Node.getInstance().getInterestManager().propagateInterests();
		}
	}
	
	public Set<String> ruleTransferrableTo(Rule r){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		substitution.put("rule", r.getUri());
		try {
			String query = this.queries.getTemplateQueries().get("rule_transferrable_to").substitute(substitution);
			Set<String> result = new HashSet<>();
			for(Map<String, String> node : Node.getInstance().getKBManager().queryModel(query)){
				result.add(node.get("node"));
			}
			return result;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}


	public void updateRules() throws UnimplementedFeatureException {
		for (String ruleURI : this.getAllRulesRef()) {
			Rule r = extractRule(ruleURI);
			this.updateRule(r);
		}
	}

	public Rule extractRule(String uri){
		LOGGER.debug("Extracting rule "+uri);
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", uri);
		String query = null;
		try {
			query = this.queries.getTemplateQueries().get("extract_rule").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		Model rule = Node.getInstance().getKBManager().constructModel(query);
		StringWriter ruleRdf = new StringWriter();
		rule.write(ruleRdf, "TTL");
		// Only one rule can match the provided URI
		return this.buildRules(ruleRdf.toString(), rule).get(0);
	}


	public List<Rule> buildRules(String rdf, Model m) {
		List<Rule> ruleList = new ArrayList<>();
		for (Map<String, String> res : QueryEngine.selectQuery(this.queries.getQueries().get("get_rule_uri"), m)) {
			LOGGER.debug("Extracting rule " + res.get("rule"));
			Rule r = new Rule();
			r.setUri(res.get("rule"));
			r.setBody(this.getRuleBody(r.getUri(), m));
			r.setHead(this.getRuleHead(r.getUri(), m));
			r.setRdf(rdf);
			ruleList.add(r);
		}
		return ruleList;
	}

	public Set<String> getRuleHead(String rule){
		Set<String> head = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		String q;
		try {
			q = this.queries.getTemplateQueries().get("get_rule_head").substitute(substitution);
			for(Map<String, String> headElement : Node.getInstance().getKBManager().queryModel(q)){
				head.add(headElement.get("head"));
			}
			return head;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Set<String> getRuleHead(String rule, Model m) {
		Set<String> head = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		String q;
		try {
			q = this.queries.getTemplateQueries().get("get_rule_head").substitute(substitution);
			for (Map<String, String> headElement : QueryEngine.selectQuery(q, m)) {
				head.add(headElement.get("head"));
			}
			return head;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}

	}

	public Set<String> getRuleBody(String rule){
		Set<String> body = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		String q;
		try {
			q = this.queries.getTemplateQueries().get("get_rule_body").substitute(substitution);
			for(Map<String, String> bodyElement : Node.getInstance().getKBManager().queryModel(q)){
				body.add(bodyElement.get("body"));
			}
			return body;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}


	public Set<String> getRuleBody(String rule, Model m) {
		Set<String> body = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		String q;
		try {
			q = this.queries.getTemplateQueries().get("get_rule_body").substitute(substitution);
			for (Map<String, String> bodyElement : QueryEngine.selectQuery(q, m)) {
				body.add(bodyElement.get("body"));
			}
			return body;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * This method is used when receiving a new rule, in order to check whether
	 * it should be applied locally or transferred.
	 * 
	 * @param rule
	 *            A RDF/TTL description of the rule, representing a shacl
	 *            CONSTRUCT
	 * @throws UnimplementedFeatureException 
	 */
	public void addRule(String rule) throws UnimplementedFeatureException {
		StringReader sr = new StringReader(rule);
		Model m = ModelFactory.createDefaultModel();
		m.read(sr, "http://example.com/ns#", "TTL");
		Node.getInstance().getKBManager().addToModel(m);
		
//		Resource report = Node.getInstance().getKBManager().validateModel();
//		if (Node.getInstance().getShapesManager().isReportConform(report.getModel())) {
//			LOGGER.debug("The KB is conform to shapes");
//		} else {
//			LOGGER.debug("The KB violates some constraints");
//			LOGGER.debug(ModelPrinter.get().print(report.getModel()));
//		}
		this.triggerReasoning(false, true);
		
//		LOGGER.trace("Deactivating node-sensitive elements");
//		Node.getInstance().getShapesManager().deactivateNodeSensitiveElements();
	}
	
	public Model triggerReasoning(Boolean activateNodeElements, Boolean deactivateNodeElements) throws UnimplementedFeatureException{
		LOGGER.trace("Starting a reasoning step");
		Model inference = Node.getInstance().getKBManager().executeRules(activateNodeElements, deactivateNodeElements);
		if(inference.size() > 0){
//			Node.getInstance().getKBManager().addToModel(inference);
			LOGGER.debug("Inferenced : " + ModelPrinter.get().print(inference));
			this.propagateRules(inference);
			return inference;
		} else {
			LOGGER.debug("Nothing inferred");
			return null;
		}
	}
	
	// Deductions made based on observations
	public void propagateRules(Model inference) throws UnimplementedFeatureException{
		Map<String, List<String>> transfers = Node.getInstance()
				.getShapesManager().extractTransferNodes(inference);
		Boolean activatedRule = false;
		if(transfers.size() == 0){
			LOGGER.debug("No transferrable rules");
		}
		for (String ruleURI : transfers.keySet()) {
			for (String nodeURI : transfers.get(ruleURI)) {
				if (E_Centralization.rulePropagationAllowed(Node.getInstance().getConfiguration().getCentralization())) {
					if(Node.getInstance().getConfiguration().getCentralization().equals(E_Centralization.CENTRAL_INDIRECT_PROCESSED)){
						List<Map<String, String>> originators = this.getOriginators(ruleURI);
						if(originators.size()> 1){
							LOGGER.error("More than one originator, not handled yet");
						}
						for(Map<String, String> originator : originators){
							this.addRuleResultConsumer(ruleURI, originator.get("originator"));
						}
					}
					Model ruleModel = this.reconstructRule(ruleURI);
					if(ruleModel == null){
						LOGGER.warn("Rule extraction failed");
					} else {
						this.transferRule(
								ruleURI,
								KBManager.serializeModel(ruleModel),
								nodeURI);
					}
				} else {
					// In the centralized case, rules that should have been
					// transmitted are activated,
					// and consumptions are propagated in their stead
					LOGGER.debug("Shifting a transfer into an activation of "+ruleURI);
//					this.shiftTransferIntoInterest(ruleURI);
					this.markRuleActivable(ruleURI);
					activatedRule = true;
//					LOGMODEL.debug(MODEL_MARKER, Node.getInstance().getKBManager().serializeModel());
					Model reinference = Node.getInstance().getKBManager().executeRules(true, true);
					Node.getInstance().getKBManager().addToModel(reinference);
					LOGGER.debug("Reinference : " + ModelPrinter.get().print(reinference));
					LOGGER.warn("Productions are not annouced (there was reinference)");
					Node.getInstance().getInterestManager().propagateInterests();
				}
			}
		}
		// If a reinference was performed, the first infernece is no longer valid, and was integrated into
		// the reinference, hence the condition. 
		if(Node.getInstance().getShapesManager().hasActivatedRule(inference) && !activatedRule){
			// If the deduction is obtained when adding the rule (and not when receiving an observation), 
			// it is never added to the KB, that is why it is done here
			Node.getInstance().getKBManager().addToModel(inference);
			Node.getInstance().getInterestManager().propagateInterests();
			LOGGER.warn("Productions are not annouced");
		}
	}
	
	public Model reconstructRule(String uri){
		Model result = null;
		Map<String, String> substitution = new HashMap<>();
		substitution.put("ruleURI", uri);
		substitution.put("originator", Node.getInstance().getUri());
		String query = "";
		E_Centralization mode = Node.getInstance().getConfiguration().getCentralization();
		try {
			if(mode.equals(E_Centralization.CENTRAL_DIRECT_PROCESSED) && Node.getInstance().getConfiguration().getRemotes().size() == 0
					|| mode.equals(E_Centralization.CENTRAL_INDIRECT_PROCESSED)){
				LOGGER.debug("Marking current node as originator");
				query = this.queries.getTemplateQueries().get("reconstruct_rule_originator").substitute(substitution);
				// The application pust also be marked as consumer of the rule's results
				substitution.put("rule", uri);
				String retrieveOriginatorQuery = this.queries.getTemplateQueries().get("get_originator").substitute(substitution);
				Map<String, String> originator = Node.getInstance().getKBManager().queryModel(retrieveOriginatorQuery).get(0);
				substitution.put("consumer", originator.get("originator"));
				String addRuleConsumerQuery = this.queries.getTemplateQueries().get("add_rule_consumer").substitute(substitution);
				Node.getInstance().getKBManager().updateModel(addRuleConsumerQuery);
//			} else if (mode.equals(E_Centralization.CENTRAL_DIRECT_PROCESSED) && Node.getInstance().getConfiguration().getRemotes().size() > 0){
//				LOGGER.debug("Marking root node as originator");
//				query = this.queries.getTemplateQueries().get("reconstruct_rule_originator").substitute(substitution);
//			} else if (mode.equals(E_Centralization.APPLICATIONS_DIRECT_PROCESSED)) {
//				query = this.queries.getTemplateQueries().get("reconstruct_rule").substitute(substitution);
			} else {
				// The default use case is the standard distribution
				// Either the orininator is the original app, or the root node, in which case the originator 
				// has been changed in the previous branch of this if
				query = this.queries.getTemplateQueries().get("reconstruct_rule").substitute(substitution);
			}
			result = Node.getInstance().getKBManager().constructModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<String> extractRulesURIs(Model m){
		List<String> uris = new ArrayList<>();
		String query = this.queries.getQueries().get("get_rule_uri");
		for(Map<String, String> row : QueryEngine.selectQuery(query, m)){
			uris.add(row.get("rule"));
		}
		return uris;
	}

	/**
	 * This method is called when receiving an information about a new node, to
	 * check wether it is necessary to update the rule application system.
	 * 
	 * @param remoteURI
	 * @return
	 */
	public void updateDownStreamNode(String node) {
		Set<String> compatibleRules = Node.getInstance().getRuleManager().findCompatibleRules(node);
		Model transferredRules = ModelFactory.createDefaultModel();
		LOGGER.debug(compatibleRules.size() + " rules can be transferred to " + node);
		if (compatibleRules.size() > 0) {
			for (String rule : compatibleRules) {
				transferredRules.add(this.collectTransferrableRuleDescription(rule));
				this.markRuleTransferred(rule, node);
				if (!this.isAggregator(rule)) {
					this.markRuleActivity(rule, false);
				}
			}
			Node.getInstance().getInterestManager().transferRules(node, compatibleRules);
			Node.getInstance().getProductionManager().transferRules(node, compatibleRules);
			StringWriter sw = new StringWriter();
			transferredRules.write(sw, "TTL");
			RulePropagationClient.sendRule(Node.getInstance().getTopologyManager().getEndpoint(node), sw.toString());
		}
	}

	/**
	 * This method is called when receiving an information about a new node, to
	 * check wether it is necessary to update the rule application system.
	 * 
	 * @param remoteURI
	 * @return
	 */
	public void updateAllDownStreamNodes() {
		for (String downstream : Node.getInstance().getTopologyManager().getDownstreamNodes()) {
			this.updateDownStreamNode(downstream);
		}
	}

	public List<String> getAllRulesRef(){
		LOGGER.trace("Listing rules references");
		SparqlSelect ss = new SparqlSelect(prefixes, "?rule", "?rule rdf:type sh:SPARQLRule");
		LOGGER.trace(ss.toString());
		List<Map<String, String>> results = null;
		try{
			results = Node.getInstance().getKBManager().queryModel(ss.toString());
			LOGGER.trace("Sparql query results : "+results);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<String> rulesRef = new ArrayList<>();
		for(Map<String, String> result : results){
			rulesRef.add(result.get("rule"));
		}
		LOGGER.debug("Rules list : "+rulesRef);
		return rulesRef;
	}

	public Model fetchRule(String ruleURI){
		return Node.getInstance().getKBManager().describeModel("DESCRIBE <"+ruleURI+">");
	}


	public String extractQuery(Model rule) {
		String where = "?r sh:construct ?q";
		SparqlSelect ss = new SparqlSelect(KBManager.prefixes, "?q", where);
		return QueryEngine.selectQuery(ss.toString(), rule).get(0).get("q");
	}

	public Model applyRule(String ruleURI){
		LOGGER.debug("Applying rule "+ruleURI);
		String rule = extractQuery(fetchRule(ruleURI));
		Model result;
		result = Node.getInstance().getKBManager().constructModel(rule);
		Node.getInstance().getKBManager().addToModel(result);
		return result;
	}

	public Set<String> getActiveRules(){
		Set<String> rules = new HashSet<>();
		String query = this.queries.getQueries().get("get_active_rules");
		for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
			rules.add(result.get("rule"));
		}
		return rules;
	}
	
	public Set<String> getActiveRulesByBody(String bodyProperty){
		Set<String> rules = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule_body", bodyProperty);
		try {
			String query = this.queries.getTemplateQueries().get("get_active_rules_by_body").substitute(substitution);
			for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
				rules.add(result.get("rule"));
			}
			return rules;
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void markRuleActivable(String ruleURI) {
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		String query;
		try {
			query = this.queries.getTemplateQueries().get("mark_rule_activable").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void markDataProcessedByRule(Model data, String rule){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		try {
			String query = this.queries.getTemplateQueries().get("mark_data_processed_rule").substitute(substitution);
			QueryEngine.updateQuery(query, data);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void shiftTransferIntoInterest(String ruleURI){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		substitution.put("host", Node.getInstance().getUri());
		String query;
		try {
			query = this.queries.getTemplateQueries().get("transfer_into_interest").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}

	public void markRuleActivity(String ruleURI, Boolean activity){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		String query;
		try {
			if(activity){
				query = this.queries.getTemplateQueries().get("activate_rule").substitute(substitution);
			} else {
				query = this.queries.getTemplateQueries().get("deactivate_rule").substitute(substitution);
			}
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}


	public void markRuleTransferred(String ruleURI, String target){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		substitution.put("target", target);
		substitution.put("self", Node.getInstance().getUri());
		String query;
		try {
			query = this.queries.getTemplateQueries().get("rule_transferred_to").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public Boolean isRuleAlreadyTransferred(String ruleURI, String target){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		substitution.put("target", target);
		String query;
		try {
			query = this.queries.getTemplateQueries().get("is_rule_transferred_to").substitute(substitution);
			return Node.getInstance().getKBManager().askModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Set<String> findCompatibleRules(String remote){
		Set<String> compatibleRules = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", Node.getInstance().getUri());
		substitution.put("remote", remote);
		try {
			String query = this.queries.getTemplateQueries().get("find_compatible_rules").substitute(substitution);
			for(Map<String, String> result :Node.getInstance().getKBManager().queryModel(query)){
				compatibleRules.add(result.get("rule"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return compatibleRules;
	}

	public Model collectTransferrableRuleDescription(String rule) {
		Map<String, String> substitution = new HashMap<>();
		substitution.put("originator_node", Node.getInstance().getUri());
		substitution.put("rule", rule);
		Model ruleModel = this.fetchRule(rule);
		try {
			QueryEngine.updateQuery(
					this.queries.getTemplateQueries().get("prepare_rule_propagation").substitute(substitution),
					ruleModel);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return ruleModel;
	}

	public Boolean isAggregator(Rule r) {
		if (r.getBody().size() > 1) {
			LOGGER.debug("For complex rules, test for complex aggregation");
			// The aggregator rule applies to rule with more than one property
			// in their body...
			return isAggregator(r.getUri());
		} else {
			LOGGER.debug("For simple rules, test for reasoning children");
			// ... for 1-prop rules, the node must just check if all its
			// children can apply the rule or some are simple sensors
			return Node.getInstance().getProductionManager()
					.hasNonReasnoningProducingChild(r.getBody().iterator().next());
		}
	}

	// TODO : shoul be possible with a single query
	public Boolean canApplyRule(Rule r) {
		Boolean canApply = true;
		for (String property : r.getBody()) {
			Boolean hasSensor = Node.getInstance().getTopologyManager().hasSensorForProperty(property);
			LOGGER.debug(Node.getInstance().getName() + " " + (hasSensor ? "has a sensor" : "doesn't have a sensor")
					+ "for " + property);
			canApply &= hasSensor;
		}
		return canApply;
	}

	public Boolean isAggregator(String rule){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		substitution.put("rule", rule);
		try {
			String query = this.queries.getTemplateQueries().get("is_aggregator").substitute(substitution);
			return Node.getInstance().getKBManager().askModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}


	public List<Map<String, String>> getOriginators(String ruleURI) {
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", ruleURI);
		try {
			String query = this.queries.getTemplateQueries().get("get_originator").substitute(substitution);
			return Node.getInstance().getKBManager().queryModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void transferRule(Rule r, String remote) {
		LOGGER.info("Transferring rule " + r.getUri() + " to " + remote);
		this.markRuleTransferred(r.getUri(), remote);
		String endpoint = Node.getInstance().getTopologyManager().getEndpoint(remote);
		// FIXME Dirty hack to mark rule origin without having to parse and
		// reserialize the RDF
		RulePropagationClient.sendRule(endpoint,
				r.getRdf() + "<" + r.getUri() + "> edr:transferredFrom <" + Node.getInstance().getUri() + ">.");
	}

	public void transferRule(String ruleURI, String ruleContent, String remote) {
		LOGGER.info("Transferring rule " + ruleURI + " to " + remote + " based on shapes");
		LOGGER.info("Rule content : "+ruleContent);
		if(!this.isRuleAlreadyTransferred(ruleURI, remote)){
			this.markRuleTransferred(ruleURI, remote);
			String endpoint = Node.getInstance().getTopologyManager().getEndpoint(remote);
			RulePropagationClient.sendRule(endpoint,ruleContent );
		} else {
			LOGGER.error("Rule already transferred");
		}	
	}
	
	public void addRuleResultConsumer(String ruleURI, String consumerURI){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("consumer", consumerURI);
		substitution.put("rule", ruleURI);
		try {
			String query = this.queries.getTemplateQueries().get("add_rule_consumer").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
}
