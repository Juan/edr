package fr.irit.melodi.edr.model;

import java.io.StringReader;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.configuration.E_Centralization;
import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.edr.out.resources.AnnouncerClient;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;


public class InterestManager {
	private static final Logger LOGGER = LogManager.getLogger(InterestManager.class);
	public static Set<Entry<String, String>> prefixes;
	private FolderManager queries;
	
	private String interestAnnounceTemplate = 
			  "PREFIX edr: <http://w3id.org/laas-iot/edr#>"
			+ "PREFIX iotl: <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>"
			+ " <NODE> edr:isInterestedIn <INTEREST>; iotl:exposes [ iotl:endpoint \"ENDPOINT\";].";
		
	static{
		prefixes = new HashSet<Entry<String, String>>();
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("sh", "<http://www.w3.org/ns/shacl#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("edr", "<https://w3id.org/laas-iot/edr#>"));
		prefixes.add(new AbstractMap.SimpleEntry<String, String>("ex", "<http://example.com/ns#>"));
	}
	
	public InterestManager(){
		try {
			this.queries = new FolderManager("queries/interests");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public void addInterestForProperty(String property){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("interest", property);
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("add_interest").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void propagateInterests() throws UnimplementedFeatureException{
		LOGGER.debug("Begining interest propagation for "+Node.getInstance().getName()+"...");
		if(Node.getInstance().getConfiguration().getCentralization().equals(E_Centralization.CENTRAL_DIRECT_RAW) &&
				Node.getInstance().getConfiguration().getRemotes().size() > 0){
			LOGGER.debug("Transferring interest of the central node");
			// For the root node, interest propagation is always the same
			Map<String, Set<String>> interests = this.listProxiedPropertiesByNodes();
			LOGGER.debug(interests);
			for(String remote : interests.keySet()){
				for(String interest : interests.get(remote)){
					this.propagateInterest(remote, interest);
				}
			}
		} else {
			// Assimilates the proxied properties, and announces them
			this.assimilateChildrenProxiedProperties();
			LOGGER.warn("There is no discrimination between proxied properties and the other regarding allchildrenannouced vs somechildrenannounced");
			Set<Entry<String, String>> interests = this.listChildrenAnnouncedProperties();
			LOGGER.debug("Propagating "+interests.size()+" interests: "+interests);
			for(Entry<String, String> proxiedProperty : interests){
				if(proxiedProperty.getKey().equals("http://w3id.org/laas-iot/edr#isInterestedIn")) {
					this.propagateInterest(Node.getInstance().getUri(), proxiedProperty.getValue());
				}
				else {
					LOGGER.error("Generic propagation unimplemented");
					throw new UnimplementedFeatureException("Trying to propagate an unexpected property");
				}
			}
		}
		LOGGER.debug("interest propagation done.");
	}
	
	public void assimilateChildrenProxiedProperties() {
		Node.getInstance().getKBManager().updateModel(
			this.queries.getQueries().get("assimilate_children_proxied_property")
		);
	}
	
	public void propagateInterest(String interested, String interestURI){
		Set<String> targets;
		if(E_Centralization.rawDataPropagation(Node.getInstance().getConfiguration().getCentralization())){
			targets = Node.getInstance().getProductionManager().getProducers(interestURI);
			LOGGER.debug(targets.size()+" nodes under "+Node.getInstance().getName()+" are producers of "+interestURI);
		} else {
			targets = Node.getInstance().getProductionManager().getPartialProducers(interestURI);
			LOGGER.debug(targets.size()+" nodes under "+Node.getInstance().getName()+" are partial producers of "+interestURI);
		}
		
		if(this.hasInterestedParent(interestURI)){
			targets = Node.getInstance().getProductionManager().getProducers(interestURI);
			LOGGER.debug(targets.size()+" nodes under "+Node.getInstance().getName()+" are producers of "+interestURI+", propagating for a parent");
		}

		for(String downstream : targets){
			String downstreamEndpoint = Node.getInstance().getTopologyManager().getEndpoint(downstream);
			if(downstreamEndpoint != null && !(downstreamEndpoint.equals(""))){
				LOGGER.debug("Propagating interests of "+interested+" for "+interestURI+" to "+downstream+" at endpoint "+downstreamEndpoint);
				String interestedEndpoint = Node.getInstance().getTopologyManager().getEndpoint(interested);
				AnnouncerClient.annouceToDownstream(
					downstreamEndpoint, 
					this.interestAnnounceTemplate
						.replaceAll("NODE", interested)
						.replaceAll("INTEREST", interestURI)
						.replaceAll("ENDPOINT", interestedEndpoint)
				);
				this.markNodeAdvertised(downstream, interestURI);
			} else {
				LOGGER.debug("Propagation of interests of "+Node.getInstance().getName()+" to "+downstream+" impossible, no endpoint");
			}
		}
	}
	
	public Boolean hasInterestedParent(String interest){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		substitution.put("interest", interest);
		try {
			String query = this.queries.getTemplateQueries().get("has_interested_parent").substitute(substitution);
			return Node.getInstance().getKBManager().askModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void markNodeAdvertised(String node, String interest){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", node);
		substitution.put("interest", interest);
		try {
			String query = this.queries.getTemplateQueries().get("mark_node_notified").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}

	public Model getInterests(){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("self", Node.getInstance().getUri());
		try {
			String query = this.queries.getTemplateQueries().get("construct_interests").substitute(substitution);
			return Node.getInstance().getKBManager().constructModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Set<Entry<String, String>> listChildrenAnnouncedProperties(){
		Set<Entry<String, String>> result = new HashSet<>();
		String query = this.queries.getQueries().get("get_children_announced_properties");
		for(Map<String, String> row : Node.getInstance().getKBManager().queryModel(query))
		{
			result.add(new AbstractMap.SimpleEntry<String, String>(row.get("predicate"), row.get("value")));
		}
		return result;
	}
	
	public Map<String, Set<String>> listProxiedPropertiesByNodes(){
		Map<String, Set<String>> result = new HashMap<>();
		String query = this.queries.getQueries().get("get_proxied_properties");
		for(Map<String, String> row : Node.getInstance().getKBManager().queryModel(query))
		{
			if(!result.containsKey(row.get("node"))){
				result.put(row.get("node"), new HashSet<>());
			}
			result.get(row.get("node")).add(row.get("property"));
		}
		return result;
	}
	
	public Map<String, Set<String>> listRemoteInterestsByType(){
		Map<String, Set<String>> result = new HashMap<>();
		String query = this.queries.getQueries().get("get_remote_interests");
		for(Map<String, String> row : Node.getInstance().getKBManager().queryModel(query))
		{
			if(!result.containsKey(row.get("interest"))){
				result.put(row.get("interest"), new HashSet<>());
			}
			result.get(row.get("interest")).add(row.get("node"));
		}
		return result;
	}
	
	public void addNewInterest(String interestTriples){
		StringReader sr = new StringReader(interestTriples);
		Model m = ModelFactory.createDefaultModel();
		m.read(sr,"http://example.com/ns#", "TTL");
		Node.getInstance().getKBManager().addToModel(m);
	}
	
	public List<String> getNodeInterests(){
		try{
			return Node.getInstance().getKBManager().getNodeInterests(Node.getInstance().getUri());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 
	 * @param interest a featureOfInterest of a property
	 * @return
	 */
	public Set<String> getInterestedNodes(String interest){
		Set<String> nodes = new HashSet<>();
		LOGGER.debug("Fetching nodes interested in "+interest);
		Map<String, String> substitution = new HashMap<>();
		substitution.put("interest", interest);
		substitution.put("self", Node.getInstance().getUri());
		String query;
		try {
			query = this.queries.getTemplateQueries().get("get_interested").substitute(substitution);
			for(Map<String, String> result : Node.getInstance().getKBManager().queryModel(query)){
				nodes.add(result.get("interested"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return nodes;
	}
	
	public void addRule(Rule r){
		for(String interest : r.getBody()){
			this.addInterestForProperty(interest);
		}
//		this.addInterestForTransferredRule(r);
		this.addRuleOriginator(r);
	}
	
	public void addRuleOriginator(Rule r){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", r.getUri());
		try {
			String query = this.queries.getTemplateQueries().get("add_rule_originator").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void addUpstreamRemote(){
		LOGGER.debug("All the required information are in the announce");
	}
	
	public void transferRules(String node, Set<String> rules){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("node", node);
		for(String rule : rules){
			substitution.put("rule", rule);
			try {
				Node.getInstance().getKBManager().updateModel(this.queries.getTemplateQueries().get("transfer_rule").substitute(substitution));
			} catch (IncompleteSubstitutionException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void updateInterests(){
		Node.getInstance().getKBManager().updateModel(this.queries.getQueries().get("adapt_interests"));
	}
	
	public void addInterestForTransferredRule(Rule r){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", r.getUri());
		try {
			Node.getInstance().getKBManager().updateModel(this.queries.getTemplateQueries().get("add_interest_transferred_rule").substitute(substitution));
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public Set<String> getRuleResultConsumers(String rule){
		Set<String> consumers = new HashSet<>();
		Map<String, String> substitution = new HashMap<>();
		substitution.put("rule", rule);
		try {
			List<Map<String, String>> result = Node.getInstance().getKBManager().queryModel(
					this.queries.getTemplateQueries().get("get_rule_result_consumers").substitute(substitution));
			for(Map<String, String> row : result){
				consumers.add(row.get("interested"));
			}
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return consumers;
	}
}
