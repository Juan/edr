package fr.irit.melodi.edr.model;

import java.util.HashSet;
import java.util.Set;

public class Rule {
	private String uri;
	// What the rule consumes
	private Set<String> body;
	// What the rule produces
	private Set<String> head;
	private String rdf;
	
	public Rule(){
		this.body = new HashSet<>();
		this.head = new HashSet<>();
	}
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public Set<String> getBody() {
		return body;
	}
	public void setBody(Set<String> body) {
		this.body = body;
	}
	public Set<String> getHead() {
		return head;
	}
	public void setHead(Set<String> head) {
		this.head = head;
	}

	public String getRdf() {
		return rdf;
	}

	public void setRdf(String rdf) {
		this.rdf = rdf;
	}
}
