package fr.irit.melodi.edr.in.resources;

import java.net.HttpURLConnection;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.irit.melodi.edr.errors.UnimplementedFeatureException;
import fr.irit.melodi.edr.node.Node;

@Path("/app")
public class ApplicationEndpoint {
	private static final Logger LOGGER = LogManager.getLogger(ApplicationEndpoint.class);
	
	@POST
	@Path("/rule")
	public Response pushRule(String rule) throws UnimplementedFeatureException{
		LOGGER.trace("Received new application rule "+rule);
		Node.getInstance().getRuleManager().addRule(rule);
		return  Response.status(HttpURLConnection.HTTP_OK).build();
	}
	
//	@POST
//	@Path("/interest")
//	public Response pushInterest(String interestTriples){
//		LOGGER.trace("Received new application interest "+interestTriples);
//		Node.getInstance().addNewInterest(interestTriples);
//		return  Response.status(HttpURLConnection.HTTP_OK).build();
//	}
}
