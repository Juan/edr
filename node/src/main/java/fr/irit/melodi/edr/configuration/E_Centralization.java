package fr.irit.melodi.edr.configuration;

public enum E_Centralization {
	
	CENTRAL_DIRECT_RAW, CENTRAL_INDIRECT_RAW, CENTRAL_DIRECT_PROCESSED, CENTRAL_INDIRECT_PROCESSED, APPLICATIONS_DIRECT_PROCESSED;

	public static E_Centralization fromString(String centralization){
		if(centralization.equals("central_direct_raw") || centralization.equals("cdr")){
			return E_Centralization.CENTRAL_DIRECT_RAW;
		} else if(centralization.equals("central_indirect_raw") || centralization.equals("cir")){
			return E_Centralization.CENTRAL_INDIRECT_RAW;
		} else if(centralization.equals("central_direct_processed") || centralization.equals("cdp")){
			return E_Centralization.CENTRAL_DIRECT_PROCESSED;
		} else if(centralization.equals("central_indirect_processed") || centralization.equals("cip")){
			return E_Centralization.CENTRAL_INDIRECT_PROCESSED;
		} else {
			return E_Centralization.APPLICATIONS_DIRECT_PROCESSED;
		}
	}
	
	public static Boolean rulePropagationAllowed(E_Centralization mode){
		return mode.equals(CENTRAL_DIRECT_PROCESSED) || mode.equals(CENTRAL_INDIRECT_PROCESSED) || mode.equals(APPLICATIONS_DIRECT_PROCESSED);
	}
	
	public static Boolean rawDataPropagation(E_Centralization mode){
		return mode.equals(CENTRAL_DIRECT_RAW) || mode.equals(CENTRAL_INDIRECT_RAW);
	}
}