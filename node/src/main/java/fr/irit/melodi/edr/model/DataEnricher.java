package fr.irit.melodi.edr.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;
import org.apache.jena.util.Locator;
import org.apache.jena.util.LocatorFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.thesmartenergy.sparql.generate.jena.SPARQLGenerate;
import com.github.thesmartenergy.sparql.generate.jena.engine.PlanFactory;
import com.github.thesmartenergy.sparql.generate.jena.engine.RootPlan;
import com.github.thesmartenergy.sparql.generate.jena.query.SPARQLGenerateQuery;

import fr.irit.melodi.edr.node.Node;
import fr.irit.melodi.edr.node.Tracer;
import fr.irit.melodi.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;

public class DataEnricher {
	private static final Logger LOGGER = LogManager.getLogger(DataEnricher.class);
	
	private static final String QUERIES_DIRECTORY = "queries/data";
	private final String BASE_DIR = "/tmp";
	private FileManager fileManager;
	private FolderManager queries;
	
	/**
	 * 
	 * @param name of the node building the enricher, to know the name of the enriched file.
	 */
	public DataEnricher(){
		this.fileManager = new FileManager();
		Locator loc;
		loc = new LocatorFile("file:"+this.BASE_DIR);
		this.fileManager.addLocator(loc);
		try {
			this.queries = new FolderManager(QUERIES_DIRECTORY);
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}
	}
	
	public String getBaseDirectory(){
		return this.BASE_DIR;
	}
	
	public Model enrichData(String data) {
		LOGGER.debug("Enriching data "+data);
		File f = new File("/tmp/"+Node.getInstance().getConfiguration().getName()+".csv");
		if(f.exists()){
			f.delete();
		} 
		
		try {
			f.createNewFile();
			FileWriter fw;
			fw = new FileWriter(f);
			fw.write(data);
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Model result = ModelFactory.createDefaultModel();
		try {
			Tracer.enrichmentStarts();
			Map<String, String> substitution = new HashMap<>();
			substitution.put("URI_BASE", Node.BASE_URI);
			substitution.put("FILE", f.getPath());
			String queryString="";
			queryString = this.queries.getTemplateQueries().get("enrich_data").substitute(substitution);
//			LOGGER.debug(queryString.replaceAll("_URI_BASE", OPADataset.URI).replaceAll("_FILE", f.getPath()).replaceAll("_URI_VOC", VOCABULARY_URI));
//			SPARQLGenerateQuery query = (SPARQLGenerateQuery) QueryFactory.create(
//					queryString.replaceAll("_URI_BASE", OPADataset.URI).replaceAll("_FILE", f.getPath()).replaceAll("_URI_VOC", VOCABULARY_URI)
//					,SPARQLGenerate.SYNTAX);
			SPARQLGenerateQuery query = (SPARQLGenerateQuery) QueryFactory.create(queryString,SPARQLGenerate.SYNTAX);
			PlanFactory factory = new PlanFactory(this.fileManager);
			RootPlan plan = factory.create(query);
			result = plan.exec();
			Tracer.enrichmentEnded(this.getObservation(result));
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		f.delete();
		this.linkEnrichedData(result);
		return result;
	}
	
	public void linkEnrichedData(Model data){
		for(String sensor : this.getFeaturedSensors(data)){
			String obsUri = this.getObservation(data);
			Map<String, String> substitution = new HashMap<>();
			substitution.put("obs", obsUri);
			substitution.put("sensor", sensor);
			try {
				String query = this.queries.getTemplateQueries().get("link_data").substitute(substitution);
				Model dataLinks = Node.getInstance().getKBManager().constructModel(query);
				data.add(dataLinks);
			} catch (IncompleteSubstitutionException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	 /**
	  * Should be used on a single observation
	  * @param data
	  * @return
	  */
	public String getObservation(Model data){
		String query = this.queries.getQueries().get("get_observation");
		List<Map<String, String>> result = QueryEngine.selectQuery(query, data);
		if(!result.isEmpty()){
			return result.get(0).get("obs");
		} else {
			return null;
		}
	}
	
	public List<String> getFeaturedSensors(Model enrichedData){
		List<String> sensors = new ArrayList<>();
		LOGGER.trace("Extracting sensors from enriched data");
		String query = this.queries.getQueries().get("get_sensors");
		for(Map<String, String> result : QueryEngine.selectQuery(query, enrichedData)){
			sensors.add(result.get("sensor"));
		}
		return sensors;
	}
	
	public List<String> getFeaturedProperties(Model enrichedData){
		List<String> properties = new ArrayList<>();
		LOGGER.debug("Extracting properties from enriched data");
		String query = this.queries.getQueries().get("get_featured_properties");
		for(Map<String, String> result : QueryEngine.selectQuery(query, enrichedData)){
			properties.add(result.get("property"));
		}
		return properties;
	}
	
	
	
	public List<String> getFeaturedPropertyClasses(Model enrichedData){
		List<String> propertyClasses = new ArrayList<>();
		LOGGER.debug("Extracting property classes from enriched data");
		String query = this.queries.getQueries().get("get_featured_property_classes");
		for(Map<String, String> result : QueryEngine.selectQuery(query, enrichedData)){
			propertyClasses.add(result.get("propertyClass"));
		}
		return propertyClasses;
	}
	
	public synchronized void deleteObservations(String sensor){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("sensor", sensor);
		try {
			String query = this.queries.getTemplateQueries().get("remove_observations").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isObservation(Model data){
		String query = this.queries.getQueries().get("is_observation");
		return QueryEngine.askQuery(query, data);
	}
	
	public synchronized void deleteDeductions(String deducer, String feature, String predicate, String object){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("deducer", deducer);
		substitution.put("feature", feature);
		substitution.put("predicate", predicate);
		substitution.put("object", object);
		try {
			String query = this.queries.getTemplateQueries().get("remove_deductions").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void deletePreviousDeductions(String deducer, String rule){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("deducer", deducer);
		substitution.put("rule", rule);
		try {
			String query = this.queries.getTemplateQueries().get("remove_rule_deductions").substitute(substitution);
			Node.getInstance().getKBManager().updateModel(query);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
	
	public void addSensorData(Model data){
		for(String sensor : this.getFeaturedSensors(data)){
			LOGGER.debug("Removing data from sensor "+sensor);
			deleteObservations(sensor);
		}
		Node.getInstance().getKBManager().addToModel(data);
	}
	
	public void addDeductionData(Model data){
//		LOGGER.trace("Extracting nodes from deduced data");
//		StringWriter sw = new StringWriter();
//		data.write(sw, "ttl");
//		LOGGER.trace("Deduction : "+sw.toString());
		String query = this.queries.getQueries().get("get_deducing_node");
		for(Map<String, String> result : QueryEngine.selectQuery(query, data)){
			LOGGER.debug("Removing data from node "+result.get("deducer"));
			deleteDeductions(result.get("deducer"), result.get("feature"), result.get("predicate"), result.get("object"));
		}
		Node.getInstance().getKBManager().addToModel(data);
	}
	
	public void addPropertyToData(Model data, String property){
		Map<String, String> substitution = new HashMap<>();
		substitution.put("property", property);
		try {
			String query = this.queries.getTemplateQueries().get("add_property_data").substitute(substitution);
			QueryEngine.updateQuery(query, data);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
	}
}
