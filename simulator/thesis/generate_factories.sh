echo "distribution local"
# python3 factory_generator.py -b distribution/distribution_0_blueprint.json -d distribution/distribution_0.ttl -o distribution/distribution_0.json
# python3 factory_generator.py -b distribution/distribution_1_blueprint.json -d distribution/distribution_1.ttl -o distribution/distribution_1.json
# python3 factory_generator.py -b distribution/distribution_2_blueprint.json -d distribution/distribution_2.ttl -o distribution/distribution_2.json
# python3 factory_generator.py -b distribution/distribution_3_blueprint.json -d distribution/distribution_3.ttl -o distribution/distribution_3.json
# python3 factory_generator.py -b distribution/distribution_4_blueprint.json -d distribution/distribution_4.ttl -o distribution/distribution_4.json

echo "scalability distrib"
python3 factory_generator.py -b scala_distributed/clone_f_0_rpi23sb_blueprint.json -d scala_distributed/clone_f_0_rpi23sb.ttl -o scala_distributed/clone_f_0_rpi23sb.json
python3 factory_generator.py -b scala_distributed/clone_f_1_rpi23sb_blueprint.json -d scala_distributed/clone_f_1_rpi23sb.ttl -o scala_distributed/clone_f_1_rpi23sb.json
python3 factory_generator.py -b scala_distributed/clone_f_2_rpi23sb_blueprint.json -d scala_distributed/clone_f_2_rpi23sb.ttl -o scala_distributed/clone_f_2_rpi23sb.json
