import json
import argparse

# SENSOR_HOST = "http://localhost"
# MACHINE_HOST = "http://localhost"
# CONVEYOR_HOST = "http://localhost"
# FLOOR_HOST = "http://localhost"
# FACTORY_HOST = "http://localhost"

SENSOR_HOST = "http://syndream.laas.fr"
MACHINE_HOST = "http://blacksad.laas.fr"
CONVEYOR_HOST = "http://syndream.laas.fr"
FLOOR_HOST = "http://rate.laas.fr"
FACTORY_HOST = "http://syndream.laas.fr"

PREFIXES = """
@prefix : <https://w3id.org/laas-iot/edr/iiot/factory#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <https://w3id.org/laas-iot/edr/iiot/factory> .
"""
BASE_PREFIX = "https://w3id.org/laas-iot/edr/iiot/factory#"
BASE_URL = "https://w3id.org/laas-iot/edr/iiot/"

TYPE_EQUIVALENCE = {
    "building":"building",
    "floor":"floor",
    "conveyor":"gallery",
    "machine":"room",
    "spark_machine":"room",
    "temperature_machine":"room"
}

def generate_machine():
    return  {"type":"machine", "host":MACHINE_HOST, "sensors":["machine_state", "product_quality"]}

def generate_conveyor(is_presence_enabled, machines_count):
    conveyor = {"type":"conveyor", "host":CONVEYOR_HOST, "sensors":["conveyor_state"], "machines":[]}
    if is_presence_enabled:
        conveyor["sensors"].append("presence")
    for i in range(machines_count):
        conveyor["machines"].append(generate_machine())
    return conveyor

def generate_floor(regular_conveyors, presence_conveyors, machines_per_conveyor, extra_sensors):
    floor = {"type":"floor", "host":FLOOR_HOST, "sensors":["presence", "luminosity"], "conveyors":[]}
    for sensor in extra_sensors:
        floor["sensors"].append(sensor)
    for i in range(regular_conveyors):
        floor["conveyors"].append(generate_conveyor(False, machines_per_conveyor))
    for i in range(presence_conveyors):
        floor["conveyors"].append(generate_conveyor(True, machines_per_conveyor))
    return floor

def generate_factory(floor_count, presence_conveyors_per_floor, regular_conveyors_per_floor, machines_per_conveyor):
    factory = {"type":"building", "host":FACTORY_HOST, "floors":[]}
    for floor in range(floor_count):
        factory["floors"].append(generate_floor(regular_conveyors_per_floor, presence_conveyors_per_floor, machines_per_conveyor, ["presence, luminosity"]))
    return factory

def build_factory_description(factory, target_file):
    floor_counter = 0
    output = {"floors":[]}
    with open(target_file, "w") as f:
        f.write(PREFIXES+"\n")
        for floor in factory["floors"]:
            for i in range(floor["count"]):
                # A new representation is created, with all counter at one and features explicited
                new_floor = {}
                new_floor["sensors"] = floor["sensors"]
                new_floor["conveyors"] = []
                new_floor["type"] = floor["type"]
                new_floor["count"]=1
                output["floors"].append(new_floor)
                floorURL = ":Floor"+str(floor_counter)
                new_floor["url"] = BASE_PREFIX+floorURL[1:]
                f.write(floorURL+" rdf:type :Floor ; :locatedIn :myFactory.\n")
                conveyor_counter = 0
                for conveyor in floor["conveyors"]:
                    for j in range(conveyor["count"]):
                        new_conveyor = {}
                        new_conveyor["sensors"] = conveyor["sensors"]
                        new_conveyor["count"]=1
                        new_conveyor["machines"]=[]
                        new_conveyor["type"] = conveyor["type"]
                        new_floor["conveyors"].append(new_conveyor)
                        conveyorURL = ":Conveyor{0}{1}".format(floor_counter, conveyor_counter)
                        new_conveyor["url"] = BASE_PREFIX+conveyorURL[1:]
                        f.write(conveyorURL+" rdf:type :Conveyor ; :locatedIn {0}.\n".format(floorURL))
                        machine_counter = 0
                        for machine in conveyor["machines"]:
                            for k in range(machine["count"]):
                                new_machine = {}
                                new_machine["type"]=machine["type"]
                                new_machine["count"]=1
                                new_machine["sensors"]=machine["sensors"]
                                new_conveyor["machines"].append(new_machine)
                                machineURL = ":Machine{0}{1}{2}".format(floor_counter, conveyor_counter, machine_counter)
                                new_machine["url"] = BASE_PREFIX+machineURL[1:]
                                f.write(machineURL+" :onConveyor {0}.\n".format(conveyorURL))
                                if machine["type"] == "temperature_machine":
                                    f.write(machineURL+" rdf:type :ColdSensitiveMachine.\n")
                                elif machine["type"] == "spark_machine":
                                    f.write(machineURL+" rdf:type :SparkGeneratingMachine.\n")
                                else:
                                    f.write(machineURL+" rdf:type :Machine.")
                                machine_counter+=1
                        conveyor_counter+=1
                floor_counter+=1
    return output

def instanciate_sensor(sensor, feature):
    return {"type":"sensor", "count":1, "host":SENSOR_HOST, "measures":sensor["type"], "feature":feature, "up":sensor["up"]}

def instanciate_node(node, host):
    return {
        "type":TYPE_EQUIVALENCE[node["type"]],
        "count":node["count"],
        "feature":node["url"],
        "host":host,
        "concentrates":[instanciate_sensor(sensor,node["url"]) for sensor in node["sensors"]]}

def generate_topology(factory, desc_file):
    topology = {"name":"myFactory", "common_knowledge":[BASE_URL+desc_file], "centralized":"adp"}
    topology["layers"] = {"type":"building", "count":1, "host":FACTORY_HOST, "concentrates":[]}
    for floor in factory["floors"]:
        topo_floor = instanciate_node(floor, FLOOR_HOST)
        for conveyor in floor["conveyors"]:
            topo_conveyor = instanciate_node(conveyor, CONVEYOR_HOST)
            for machine in conveyor["machines"]:
                topo_machine = instanciate_node(machine, MACHINE_HOST)
                topo_conveyor["concentrates"].append(topo_machine)
            topo_floor["concentrates"].append(topo_conveyor)
        topology["layers"]["concentrates"].append(topo_floor)
    return topology

def spread_sensors(root):
    sensors = []
    for node in [x for x in root["concentrates"] if x["type"] != "sensor"]:
        sensors+=spread_sensors(node)
    i=0
    while i < len(sensors):
        sensors[i]["up"]-=1
        if sensors[i]["up"]==0:
            root["concentrates"].append(sensors[i])
            del(sensors[i])
        else:
            # In the other case, the list is shortened, no need to increment
            i+=1
    current_sensors = [x for x in root["concentrates"] if x["type"] == "sensor"]
    i=0
    while i < len(root["concentrates"]):
        if root["concentrates"][i]["type"] == "sensor" and root["concentrates"][i]["up"] > 0:
            sensors.append(root["concentrates"][i])
            del(root["concentrates"][i])
        else:
            # In the other case, the list is shortened, no need to increment
            i+=1
    return sensors



parser = argparse.ArgumentParser()
parser.add_argument('-b', '--blueprint', help="JSON in which the factory is described", type=str)
parser.add_argument('-d', '--desc_file', help="the ttl file in which the topology is described", type=str)
parser.add_argument('-o', '--output', help="File in which the topology is described", type=str)

args = parser.parse_args()

description_file = args.desc_file
factory = {}
with open(args.blueprint, "r") as f:
    factory = json.load(f)
factory_desc = build_factory_description(factory, description_file)
# with open("test", "w") as f:
#     json.dump(factory_desc, f, indent=2)
topology = generate_topology(factory_desc, description_file)
# with open("test", "w") as f:
#     json.dump(topology, f, indent=2)
spread_sensors(topology["layers"])
with open(args.output, "w") as f:
    json.dump(topology, f, indent=2)
