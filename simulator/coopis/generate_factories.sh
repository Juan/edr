# echo "scalability local 0"
# python3 factory_generator.py -b scala_syndream/clone_f_0_blueprint.json -d scala_syndream/clone_f_0.ttl -o scala_syndream/clone_f_0.json
# echo "scalability local 1"
# python3 factory_generator.py -b scala_syndream/clone_f_1_blueprint.json -d scala_syndream/clone_f_1.ttl -o scala_syndream/clone_f_1.json
# echo "scalability local 2"
# python3 factory_generator.py -b scala_syndream/clone_f_2_blueprint.json -d scala_syndream/clone_f_2.ttl -o scala_syndream/clone_f_2.json
#
# echo "scalability distributed 0"
# python3 factory_generator.py -b scala_distributed/clone_f_0_rpi23sb_blueprint.json -d scala_distributed/clone_f_0_rpi23sb.ttl -o scala_distributed/clone_f_0_rpi23sb.json
# echo "scalability distributed 1"
# python3 factory_generator.py -b scala_distributed/clone_f_1_rpi23sb_blueprint.json -d scala_distributed/clone_f_1_rpi23sb.ttl -o scala_distributed/clone_f_1_rpi23sb.json
# echo "scalability distributed 2"
# python3 factory_generator.py -b scala_distributed/clone_f_2_rpi23sb_blueprint.json -d scala_distributed/clone_f_2_rpi23sb.ttl -o scala_distributed/clone_f_2_rpi23sb.json
#
# echo "spe baseline"
# python3 factory_generator.py -b specialisation/specialisation_baseline_blueprint.json -d specialisation/specialisation_baseline.ttl -o specialisation/specialisation_baseline.json
# echo "spe implem"
# python3 factory_generator.py -b specialisation/specialisation_implementation_blueprint.json -d specialisation/specialisation_implementation.ttl -o specialisation/specialisation_implementation.json

# echo "distribution local 0"
# python3 factory_generator.py -b distribution_syndream/distribution_0_blueprint.json -d distribution_syndream/distribution_0.ttl -o distribution_syndream/distribution_0.json
# echo "distribution local 1"
# python3 factory_generator.py -b distribution_syndream/distribution_1_blueprint.json -d distribution_syndream/distribution_1.ttl -o distribution_syndream/distribution_1.json
# echo "distribution local 2"
# python3 factory_generator.py -b distribution_syndream/distribution_2_blueprint.json -d distribution_syndream/distribution_2.ttl -o distribution_syndream/distribution_2.json
# echo "distribution local 3"
# python3 factory_generator.py -b distribution_syndream/distribution_3_blueprint.json -d distribution_syndream/distribution_3.ttl -o distribution_syndream/distribution_3.json

echo "distribution distributed 0"
python3 factory_generator.py -b distribution_distributed/distribution_0_blueprint.json -d distribution_distributed/distribution_0.ttl -o distribution_distributed/distribution_0.json
echo "distribution distributed 1"
python3 factory_generator.py -b distribution_distributed/distribution_1_blueprint.json -d distribution_distributed/distribution_1.ttl -o distribution_distributed/distribution_1.json
echo "distribution distributed 2"
python3 factory_generator.py -b distribution_distributed/distribution_2_blueprint.json -d distribution_distributed/distribution_2.ttl -o distribution_distributed/distribution_2.json
echo "distribution distributed 3"
python3 factory_generator.py -b distribution_distributed/distribution_3_blueprint.json -d distribution_distributed/distribution_3.ttl -o distribution_distributed/distribution_3.json
