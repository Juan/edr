import json
import logging
import sys
import os
import glob
import argparse
#from visualize_topology import visualize

logging.basicConfig(level=logging.DEBUG)

SENSOR_INITIAL_SLEEP=120

BASE_URL = "http://example.com/ns#"

def serialize_number(number):
    if number < 10:
        return "000"+str(number)
    elif number < 100:
        return "00"+str(number)
    elif number < 1000:
        return "0"+str(number)
    else:
        return str(number)

def create_json(topology_name, node, output):
    dirpath = ""
    if output == None:
        dirpath = topology_name+"/"+node["host"].split("/")[-1]
    else:
        dirpath = output+"/"+node["host"].split("/")[-1]
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
    with open(dirpath+"/"+node["name"]+".json", "w") as f:
        json.dump(node, f)

def parametrize_sensor(node):
    if node["measures"] == "luminosity":
        node["measureType"] = "float"
        node["lowerBound"] = 0
        node["upperBound"] = 1500
    elif node["measures"] == "humidity":
        node["measureType"] = "float"
        node["lowerBound"] = 73.0
        node["upperBound"] = 94.0
    elif node["measures"] == "noise":
        node["measureType"] = "float"
        node["lowerBound"] = 10.0
        node["upperBound"] = 90.0
    elif node["measures"] == "power_consumption":
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 450.0
    elif node["measures"] == "power_production":
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 75000.0
    elif node["measures"] == "power_redistribution":
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 450.0
    elif node["measures"] == "smoke":
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 100.0
    elif node["measures"] == "temperature":
        node["measureType"] = "float"
        node["lowerBound"] = 17.0
        node["upperBound"] = 30.0
    elif node["measures"] == "temperature_request":
        node["measureType"] = "float"
        node["lowerBound"] = 20.0
        node["upperBound"] = 25.0
    elif node["measures"] == "presence":
        node["measureType"] = "boolean"
        node["rate"] = 0.5
    elif node["measures"] == "machine_state":
        node["measureType"] = "boolean"
        node["rate"] = 0.5
    elif node["measures"] == "conveyor_state":
        node["measureType"] = "boolean"
        node["rate"] = 0.5
    elif node["measures"] == "supervisor_presence":
        node["measureType"] = "boolean"
        node["rate"] = 0.8
    elif node["measures"] == 'openness':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 100.0
    elif node["measures"] == 'wind_direction':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 360.0
    elif node["measures"] == 'wind_speed':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 6.0
    elif node["measures"] ==  'power':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 450.0
    elif node["measures"] == 'rotation':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 100.0
    elif node["measures"] == 'pluviometry':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 150.0
    elif node["measures"] == 'electrical_tension':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 450
    elif node["measures"] == 'pyranometry':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 615.0
    elif node["measures"] == 'hygrometry':
        node["measureType"] = "float"
        node["lowerBound"] = 73.0
        node["upperBound"] = 94.0
    elif  node["measures"] == 'athmospheric_pressure':
        node["measureType"] = "float"
        node["lowerBound"] = 989.0
        node["upperBound"] = 993.0
    elif node["measures"] == 'particle_level':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 40.0
    elif node["measures"] == 'product_quality':
        node["measureType"] = "float"
        node["lowerBound"] = 97.5
        node["upperBound"] = 100.0
    elif node["measures"] == 'conveyor_speed':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 50.0
    elif node["measures"] == 'machine_speed':
        node["measureType"] = "float"
        node["lowerBound"] = 0.0
        node["upperBound"] = 50.0

def set_host(node, topology, upper_node):
    if "host" in topology:
        node["host"] = topology["host"]
    elif "host" in upper_node:
        node["host"] = upper_node["host"]
    else:
        node["host"] = "http://localhost"

def set_remote(node, topology, upper_node):
    if not "remotes" in node:
        node["remotes"]=[]
    if "host" in upper_node:
        node["remotes"].append(upper_node["host"]+":"+str(6000+upper_node["id"]))
    else:
        node["remotes"].append("http://localhost"+":"+str(6000+upper_node["id"]))

def find_datafile(sensor_id, data_folder):
    for datafile in glob.glob(data_folder+"/*.csv"):
        if sensor_id in datafile:
            return datafile
    return None


# Topology is the map extracted from the json config describing the nodes to generate, node_list is a list used to be able to display the result of the generation,
def parse_topology(topology_name, common_knowledge, topology, node_list, upper_node, node_counter, centralization, output, data, replay):
    spawned_nodes = 0
    # Creation of the new node level (as many as counted)
    for i in range(topology["count"]):
        spawned_nodes += 1
        node_id = node_counter+spawned_nodes
        node={}
        if("concentrates" in topology):
            # Case of a non-sensor node, with children
            node = {
                "id":node_id,
                "name":topology["type"]+serialize_number(node_id),
                "port":6000+node_id,
                "queries":"queries/devices",
                "local":"false",
                "tbox":common_knowledge,
                "sensors":[],
                "reasoning":True,
                "centralization":centralization
            }
            # If no host is defined, it is inherited from the parent node
            set_host(node, topology, upper_node)
            set_remote(node, topology, upper_node)
            if(upper_node["id"] == 0):
                node["remotes"]=[]
            if("feature" in topology):
                if topology["feature"] == "self":
                    node["feature"] = BASE_URL+node["name"]+"feature"
                elif topology["feature"] == "parent":
                    node["feature"] = BASE_URL+upper_node["name"]+"feature"
                else:
                    node["feature"] = topology["feature"]
            # Creation of their children
            for child_id in range(len(topology["concentrates"])):
                spawned_nodes += parse_topology(topology_name, common_knowledge, topology["concentrates"][child_id], node_list, node, node_counter+spawned_nodes, centralization, output, data, replay)
        else:
            node = {
                "id":node_id,
                "name":topology["type"]+serialize_number(node_id),
                "port":9000+node_id,
                "cycleDuration":800,
                "measures":topology["measures"],
                "reasoning":False,
                "initialSleep":SENSOR_INITIAL_SLEEP
            }
            set_host(node, topology, upper_node)
            sensor_ref = { "endpoint":node["host"]+":"+str(node["port"]),
                "produces":node["measures"] }
            if "sensor_id" in topology:
                node["sensorId"] = topology["sensor_id"]
                node["datafile"] = find_datafile(topology["sensor_id"], data)
                #node["feature"] = topology["feature"]
                sensor_ref["feature"] =  topology["feature"]
            if "cycleDuration" in topology:
                node["cycleDuration"] = topology["cycleDuration"]
            if replay != None:
                node["datafile"] = replay+node["name"]+".log"
            if "feature" in topology:
                sensor_ref["feature"] = topology["feature"]
            upper_node["sensors"].append(sensor_ref)
            parametrize_sensor(node)
        create_json(topology_name, node, output)
        node_list[node["id"]]=node
    return spawned_nodes

parser = argparse.ArgumentParser(description='Formats adream sensor data to be replayed by an EDR simulation.')
parser.add_argument('-t', '--topology', help="The json topology file", type=str)
parser.add_argument('-d', '--data', help="The folder containing the csv files to be replayed", type=str)
parser.add_argument('-o', '--output', help="The folder to which results are sent", type=str)
# CENTRAL_DIRECT_RAW, CENTRAL_INDIRECT_RAW, CENTRAL_DIRECT_PROCESSED, CENTRAL_INDIRECT_PROCESSED, APPLICATIONS_DIRECT_PROCESSED
parser.add_argument('-c', '--centralization', help="Centralization mode (cir, cdr, cip, cdp, adp)", type=str)
parser.add_argument('-r', '--replay', help="Folder where sensor logs is stored", type=str)

args = parser.parse_args()

topology = {}
# if len(sys.argv) != 3:
#     exit("usage : "+sys.argv[0]+" <topology file>")
if args.topology == None:
    exit(args.help)

topo_filename = args.topology
topo_name = topo_filename.split(".")[0]

for node in glob.glob(args.output+"/*/*.json"):
    os.remove(node)

with open(topo_filename, "r", encoding='utf-8') as topology_file:
    topology = json.loads(topology_file.read())
    node_list = {}
    centralization = topology["centralized"]
    common_knowledge = []
    if "common_knowledge" in topology:
        common_knowledge = topology["common_knowledge"]
    if "centralization" in args.__dict__:
        centralization = args.centralization
    parse_topology(topo_name, common_knowledge, topology["layers"], node_list, {"id":0}, 0, centralization, args.output, args.data, args.replay)
    print("Generated "+str(len(node_list))+" nodes")
    #visualize(node_list, topo_name)
