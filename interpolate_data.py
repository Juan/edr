import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import sys
import os
from datetime import datetime, time, timedelta, timezone
import dateutil.parser
import glob
import argparse

def fill_day(day):
    delta = timedelta(minutes=1)
    # Starts at 00:00:00
    cur_time = datetime.combine(list(day.keys())[0].date(), time(tzinfo=timezone(timedelta(hours=1))))
    for m in range(1440):
        cur_time = cur_time + delta
        if not cur_time in day:
            day[cur_time] = None

parser = argparse.ArgumentParser(description='Interpolates missing CSV data to have the same number of records for all sensors.')
parser.add_argument('-d', '--data', help="the folder containing the csv files to be interpolated", type=str)
args = parser.parse_args()

for file in glob.glob(args.data+"/*.csv"):
    print("Completing "+file)
    records = None
    with open(file) as f:
        records = csv.reader(f)
        full_day = {}
        example_record = []
        for r in records:
            full_day[dateutil.parser.parse(r[0])] = float(r[2])
            example_record = r
    fill_day(full_day)
    df = pd.DataFrame.from_dict(full_day, orient="index")
    df = df.sort_index()
    # df = df.interpolate()
    with open(args.data+"/tmp.csv", "w") as f:
        for row in df.itertuples(name=None):
            f.write("{0}, {1}, {2}, {3}, {4}\n".format(row[0], example_record[1], row[1], example_record[3], example_record[4]))
    os.rename(args.data+"/tmp.csv", file)
