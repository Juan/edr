prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:R3Spot
	edr:hasTransferShape ex:R3TransferShape ;
	edr:hasApplyShape ex:R3ApplicableShape ;
	edr:hasDeliveryShape ex:R3ResultTransferShape ;
	edr:hasDeductionShape ex:R3ActiveShape.

ex:R3TransferShape
	a sh:NodeShape ;
	a edr:TransferShape;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:Temperature, adr:Humidity, adr:Luminosity ;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{ex:R3 edr:transferredTo $this.}
									UNION
									{ex:R3 edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:R3RuleTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R3TransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:R3 edr:transferableTo $this.
				ex:R3 edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:R3ApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:HostNode ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>
				PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT $this {
					FILTER NOT EXISTS {
						ex:R3 edr:isRuleActivable "true"^^xsd:boolean.
					}
					FILTER NOT EXISTS {
						$this a lmu:HostNode.
						$this lmu:hasDownstreamNode ?temperatureProvider, ?humidityProvider, ?luminosityProvider.
						?temperatureProvider edr:producesDataOn adr:Temperature.
						?humidityProvider edr:producesDataOn adr:Humidity.
						?luminosityProvider edr:producesDataOn adr:Luminosity.
						FILTER NOT EXISTS {
              ex:R3 edr:isRuleActive "true"^^xsd:boolean.
            }
						FILTER EXISTS {
							$this lmu:hasDownstreamNode ?lowerNode.
							FILTER(?lowerNode = ?luminosityProvider || ?lowerNode = ?temperatureProvider || ?lowerNode = ?humidityProvider)
							FILTER NOT EXISTS {
								?lowerNode edr:producesDataOn adr:Temperature, adr:Luminosity, adr:Humidity.
							}
						}
					}
        }
        """ ;
  ].

ex:R3ApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R3ApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:Luminosity, adr:Humidity, adr:Temperature.
				$this edr:producesDataOn ex:Symptom3.
				?partialDataProvider edr:partialDataProvider ?partialProduction.
				ex:R3 edr:isRuleActive "true"^^xsd:boolean.
				ex:myAbstractApp edr:consumesResult ex:R3.
			} WHERE {
				$this a lmu:HostNode.
				{
					$this lmu:hasDownstreamNode ?partialDataProvider.
					?partialDataProvider edr:producesDataOn ?partialProduction.
					FILTER NOT EXISTS {
						?partialDataProvider edr:producesDataOn adr:Luminosity, adr:Humidity, adr:Temperature.
					}
				} UNION {
					ex:R3 edr:isRuleActivable "true"^^xsd:boolean.
				}
				ex:R3 edr:ruleOriginatedFrom ?originator.
				OPTIONAL{ex:R3 edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].

ex:R3ActiveShape
	a sh:NodeShape ;
	a edr:ActiveShape ;
	sh:targetClass lmu:HostNode ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>
				PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT $this
				WHERE {
					FILTER NOT EXISTS {
						$this a lmu:HostNode.
						ex:R3 edr:isRuleActive "true"^^xsd:boolean.
					}
        }
        """ ;
  ].


ex:R3ResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:isInterestedIn ex:Symptom3 ;
							 	lmu:hasDownstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{$this edr:consumesResult ex:R3.}
								}
						}
        }
        """ ;
  ].

ex:R3ResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R3ResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:R3.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].


ex:R3Shape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	rdfs:comment "Infers whether the conditions are confortable for office work";
	sh:rule ex:R3 .

	ex:myAbstractApp  a lmu:Application;
		iotl:exposes [
			iotl:endpoint "http://syndream.laas.fr:8005";
		].

ex:R3	a sh:SPARQLRule ;
	rdfs:comment "T, L, H -> S3";
	sh:condition ex:R3ActiveShape ;
	edr:ruleOriginatedFrom ex:myAbstractApp;
	edr:originatingEndpoint "http://syndream.laas.fr:8005";
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX opa:<https://w3id.org/laas-iot/adream#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
		PREFIX adr: <https://w3id.org/laas-iot/adream#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?feature;
				rdf:predicate rdf:type;
				rdf:object ex:Symptom3;
				edr:deducedAt ?now;
				edr:deducedBy ?deducer;
				edr:deducedWith ex:R3;
				edr:deducedFrom ?t_obs, ?l_obs, ?h_obs.
		} WHERE {
			?t rdf:type/rdfs:subClassOf* adr:Temperature;
				ssn:isPropertyOf ?feature.
			?t_obs ssn:observationResult/ssn:hasValue/dul:hasDataValue ?temperatureValue;
				ssn:observedProperty ?t;
				ssn:observedBy ?t_sensor.
			FILTER(?temperatureValue > '20.0'^^xsd:float)

			?l rdf:type/rdfs:subClassOf* adr:Luminosity;
				ssn:isPropertyOf ?feature.
			?l_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?luminosityValue;
				ssn:observedProperty ?l;
				ssn:observedBy ?l_sensor.
			FILTER(?luminosityValue > '150.0'^^xsd:float)

			?h rdf:type/rdfs:subClassOf* adr:Humidity;
				ssn:isPropertyOf ?feature.
			?h_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?humidityValue;
				ssn:observedProperty ?h;
				ssn:observedBy ?h_sensor.
			FILTER(?humidityValue > '35.0'^^xsd:float)

			?deducer a lmu:HostNode.

			FILTER NOT EXISTS {
				?t_obs edr:usedForDeductionBy ex:R3.
				?l_obs edr:usedForDeductionBy ex:R3.
				?h_obs edr:usedForDeductionBy ex:R3.
			}

			FILTER NOT EXISTS {
				?deduction  edr:deducedWith ex:R3;
					edr:deducedFrom ?t_obs, ?l_obs, ?h_obs.
			}

			BIND(URI(CONCAT('http://example.com/ns#R3_deduction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
ex:Symptom3 rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
