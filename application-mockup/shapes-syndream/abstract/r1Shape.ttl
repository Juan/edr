prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:R1Spot
	edr:hasTransferShape ex:R1TransferShape ;
	edr:hasApplyShape ex:R1ApplicableShape ;
	edr:hasDeliveryShape ex:R1ResultTransferShape ;
	edr:hasDeductionShape ex:R1ActiveShape.

ex:R1TransferShape
	a sh:NodeShape ;
	a edr:TransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:Temperature, adr:Luminosity ;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{ex:R1 edr:transferredTo $this.}
									UNION
									{ex:R1 edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:R1Transfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R1TransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:R1 edr:transferableTo $this.
				ex:R1 edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:R1ApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	sh:targetClass lmu:HostNode ;
	a edr:NodeSensitiveComponent;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>
				PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT $this {
					FILTER NOT EXISTS {
						ex:R1 edr:isRuleActivable "true"^^xsd:boolean.
					}
					FILTER NOT EXISTS {
						$this a lmu:HostNode.
						$this lmu:hasDownstreamNode ?temperatureProvider, ?luminosityProvider.
						?temperatureProvider edr:producesDataOn adr:Temperature.
						?luminosityProvider edr:producesDataOn adr:Luminosity.
						FILTER NOT EXISTS {
              ex:R1 edr:isRuleActive "true"^^xsd:boolean.
            }
						FILTER EXISTS {
							$this lmu:hasDownstreamNode ?lowerNode.
							FILTER(?lowerNode = ?luminosityProvider || ?lowerNode = ?temperatureProvider)
							FILTER NOT EXISTS {
								?lowerNode edr:producesDataOn adr:Temperature, adr:Luminosity.
							}
						}
					}
        }
        """ ;
  ].

ex:R1ApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R1ApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:Luminosity, adr:Temperature.
				$this edr:producesDataOn ex:Symptom1.
				?partialDataProvider edr:partialDataProvider ?partialProduction.
				ex:R1 edr:isRuleActive "true"^^xsd:boolean.
				?originator edr:consumesResult ex:R1.
			} WHERE {
				$this a lmu:HostNode.
				{
					$this lmu:hasDownstreamNode ?partialDataProvider.
					?partialDataProvider edr:producesDataOn ?partialProduction.
					FILTER NOT EXISTS {
						?partialDataProvider edr:producesDataOn adr:Luminosity, adr:Temperature.
					}
				} UNION {
					ex:R1 edr:isRuleActivable "true"^^xsd:boolean.
				}
				ex:R1 edr:ruleOriginatedFrom ?originator.
				OPTIONAL{ex:R1 edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].


ex:R1ActiveShape
	a sh:NodeShape ;
	a edr:ActiveShape ;
	sh:targetClass lmu:HostNode ;
	a edr:ContentSensitiveComponent;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>
				PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT $this
				WHERE {
					FILTER NOT EXISTS {
						$this a lmu:HostNode.
						ex:R1 edr:isRuleActive "true"^^xsd:boolean.
					}
        }
        """ ;
  ].

ex:R1ResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:isInterestedIn ex:Symptom1 ;
							 	lmu:hasDownstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{$this edr:consumesResult ex:R1.}
								}
						}
        }
        """ ;
  ].

ex:R1ResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:R1ResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:R1.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].

ex:R1Shape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	rdfs:comment "Infers whether the conditions are confortable for office work";
	sh:rule ex:R1 .

# ex:R1 rdf:type sh:SPARQLRule ;
# 	edr:hasRuleBody adr:Temperature, adr:Luminosity;
# 	edr:hasRuleHead ex:Symptom1;
# 	edr:ruleOriginatedFrom ex:myAbstractApp;
# 	edr:originatingEndpoint "http://syndream.laas.fr:8005";

ex:myAbstractApp  a lmu:Application;
	iotl:exposes [
		iotl:endpoint "http://syndream.laas.fr:8005";
	].

ex:R1	a sh:SPARQLRule ;
	rdfs:comment "T, L -> C1";
	sh:condition ex:R1ActiveShape ;
	edr:ruleOriginatedFrom ex:myAbstractApp ;
	edr:originatingEndpoint "http://syndream.laas.fr:8005" ;
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX opa:<https://w3id.org/laas-iot/adream#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
		PREFIX adr: <https://w3id.org/laas-iot/adream#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?feature;
				rdf:predicate rdf:type;
				rdf:object ex:Symptom1;
				edr:deducedAt ?now;
				edr:deducedBy $this;
				edr:deducedWith ex:R1;
				edr:deducedFrom ?t_obs, ?l_obs.
		} WHERE {
			?t rdf:type/rdfs:subClassOf* adr:Temperature;
				ssn:isPropertyOf ?feature.
			?t_obs ssn:observationResult/ssn:hasValue/dul:hasDataValue ?temperatureValue;
				ssn:observedProperty ?t;
				ssn:observedBy ?temperature_sensor.
			FILTER(?temperatureValue <= '25.0'^^xsd:float)

			?l rdf:type/rdfs:subClassOf* adr:Luminosity;
				ssn:isPropertyOf ?feature.
			?l_obs	ssn:observationResult/ssn:hasValue/dul:hasDataValue ?luminosityValue;
				ssn:observedProperty ?l;
				ssn:observedBy ?luminosity_sensor.
			FILTER(?luminosityValue < '800.0'^^xsd:float)

			$this a lmu:HostNode.

			FILTER NOT EXISTS {
				?t_obs edr:usedForDeductionBy ex:R1.
				?l_obs edr:usedForDeductionBy ex:R1.
			}
			FILTER NOT EXISTS {
				?otherDeduction edr:deducedWith ex:R1;
					edr:deducedFrom ?t_obs, ?l_obs.
			}

			BIND(URI(CONCAT('http://example.com/ns#R1_deduction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
ex:Symptom1 rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
