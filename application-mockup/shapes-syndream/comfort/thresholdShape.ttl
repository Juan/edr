prefix san:   <http://www.irit.fr/recherches/MELODI/ontologies/SAN#>
prefix xsd:   <http://www.w3.org/2001/XMLSchema#>
prefix fn:    <http://w3id.org/sparql-generate/fn/>
prefix iter:  <http://w3id.org/sparql-generate/iter/>
prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
prefix adr:   <https://w3id.org/laas-iot/adream#>
prefix iotl:  <http://iot.ee.surrey.ac.uk/fiware/ontologies/iot-lite#>
prefix ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
prefix ioto:  <http://www.irit.fr/recherches/MELODI/ontologies/IoT-O#>
prefix edr:   <http://w3id.org/laas-iot/edr#>
prefix ex:    <http://example.com/ns#>
prefix sh:    <http://www.w3.org/ns/shacl#>
prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix lmu:   <http://w3id.org/laas-iot/lmu#>
prefix dul:   <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
prefix time:  <http://www.w3.org/2006/time#>

ex:Threshold
	edr:hasTransferShape ex:ThresholdTransferShape ;
	edr:hasApplyShape ex:ThresholdApplicableShape ;
	edr:hasDeliveryShape ex:ThresholdRuleResultTransferShape ;
	edr:hasDeductionShape ex:ThresholdActiveShape.

ex:ThresholdTransferShape
	a sh:NodeShape ;
	a edr:TransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:producesDataOn adr:Temperature;
							 	lmu:hasUpstreamNode [
									a lmu:HostNode;
								].
                FILTER NOT EXISTS {
									{ex:ThresholdRule edr:transferredTo $this.}
									UNION
									{ex:ThresholdRule edr:transferableTo $this.}
								}
						}
        }
        """ ;
  ].

ex:ThresholdTransferRule
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:ThresholdTransferShape ;
		sh:construct """
			PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
			PREFIX ex:<http://example.com/ns#>
			PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				ex:ThresholdRule edr:transferableTo $this.
				ex:ThresholdRule edr:transferredFrom ?host.
			} WHERE {
				$this lmu:hasUpstreamNode ?host.
				?host a lmu:HostNode.
			}
		""";
	].

ex:ThresholdApplicableShape
	a sh:NodeShape ;
	a edr:ApplicableShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:HostNode ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
        prefix adr:   <https://w3id.org/laas-iot/adream#>
        prefix ex:    <http://example.com/ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

        SELECT $this {
					FILTER NOT EXISTS {
						ex:ThresholdRule edr:isRuleActivable "true"^^xsd:boolean.
					}
					FILTER NOT EXISTS {
						$this a lmu:HostNode;
							lmu:hasDownstreamNode ?temperatureProvider.
						?temperatureProvider edr:producesDataOn adr:Temperature;
								a ssn:Sensor.
            FILTER NOT EXISTS {
              ex:ThresholdRule edr:isRuleActive "true"^^xsd:boolean.
            }
					}
        }
        """ ;
  ].

ex:ThresholdApplicantRule
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:ThresholdApplicableShape ;
		sh:construct """
			prefix adr:   <https://w3id.org/laas-iot/adream#>
			PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			prefix ex:    <http://example.com/ns#>
			prefix lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:isInterestedIn adr:Temperature.
				$this edr:producesDataOn ex:AboveThresholdTemperature.
				ex:ThresholdRule edr:isRuleActive "true"^^xsd:boolean.
				?originator edr:consumesResult ex:ThresholdRule.
			} WHERE {
				$this a lmu:HostNode.
				ex:ThresholdRule edr:ruleOriginatedFrom ?originator.
				OPTIONAL{ex:ThresholdRule edr:isRuleActive "false"^^xsd:boolean.}
			}
		""";
	].

ex:ThresholdActiveShape		a sh:NodeShape ;
		sh:targetClass lmu:HostNode ;
		a edr:ActiveShape ;
	  sh:sparql [
	    sh:select """
	        PREFIX edr: <http://w3id.org/laas-iot/edr#>
	        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
	        PREFIX ssn:   <http://purl.oclc.org/NET/ssnx/ssn#>
	        prefix adr:   <https://w3id.org/laas-iot/adream#>
					prefix ex:    <http://example.com/ns#>
					PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	        SELECT $this
					WHERE {
						FILTER NOT EXISTS {
							$this a lmu:HostNode.
							ex:ThresholdRule edr:isRuleActive "true"^^xsd:boolean.
						}
	        }
	        """ ;
	  ].

ex:ThresholdRuleResultTransferShape
	a sh:NodeShape ;
	a edr:ResultTransferShape ;
	a edr:NodeSensitiveComponent;
	sh:targetClass lmu:Node ;
  sh:sparql [
    sh:select """
        PREFIX edr: <http://w3id.org/laas-iot/edr#>
        PREFIX lmu: <http://w3id.org/laas-iot/lmu#>
				prefix ex:    <http://example.com/ns#>

        SELECT $this {
						FILTER NOT EXISTS {
							$this a lmu:Node ;
								edr:isInterestedIn ex:AboveThresholdTemperature ;
							 	lmu:hasDownstreamNode [
									a lmu:HostNode;
								].
								FILTER NOT EXISTS {
									{$this edr:consumesResult ex:ThresholdRule.}
								}
						}
        }
        """ ;
  ].

ex:ThresholdRuleResultTransfer
	a sh:NodeShape ;
	sh:targetClass lmu:Node ;
	sh:rule [
		a sh:SPARQLRule ;
		sh:condition ex:ThresholdRuleResultTransferShape ;
		sh:construct """
			PREFIX ex:<http://example.com/ns#>
			PREFIX edr: <http://w3id.org/laas-iot/edr#>
			PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
			CONSTRUCT {
				$this edr:consumesResult ex:ThresholdRule.
			} WHERE {
				$this a lmu:Node ;
			}
		""";
	].

ex:myComfortApp a lmu:Application ;
	iotl:exposes [
		iotl:endpoint "http://syndream.laas.fr:8002";
	].

ex:ThresholdRuleShape
	a sh:NodeShape ;
	sh:targetClass lmu:HostNode ;
	sh:rule ex:ThresholdRule .

ex:ThresholdRule rdf:type sh:SPARQLRule ;
	rdfs:comment "Infers places where the temperature is above a fixed threshold";
	edr:ruleOriginatedFrom ex:myComfortApp;
	edr:originatingEndpoint "http://syndream.laas.fr:8002";
  sh:condition ex:ThresholdActiveShape ;
	sh:construct """
		PREFIX ssn:<http://purl.oclc.org/NET/ssnx/ssn#>
		PREFIX ex:<http://example.com/ns#>
		PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
		PREFIX opa:<https://w3id.org/laas-iot/adream#>
		PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
		PREFIX dul: <http://www.ontologydesignpatterns.org/ont/dul/DUL.owl#>
		PREFIX edr: <http://w3id.org/laas-iot/edr#>
		PREFIX lmu:   <http://w3id.org/laas-iot/lmu#>
		CONSTRUCT {
			?deduction a rdf:Statement;
				rdf:subject ?property;
				rdf:predicate rdf:type;
				rdf:object ex:AboveThresholdTemperature;
				edr:deducedAt ?now;
				edr:deducedBy $this;
				edr:deducedWith ex:ThresholdRule;
				edr:deducedFrom ?obsURI.
		} WHERE {
			?property rdf:type/rdfs:subClassOf* <https://w3id.org/laas-iot/adream#Temperature>.
			?obsURI a ssn:Observation;
				ssn:observationResult ?sensorOutputURI;
				ssn:observedProperty ?property;
				ssn:observedBy ?sensor.
			?sensorOutputURI ssn:hasValue ?obsValURI.
			?obsValURI dul:hasDataValue ?value.
			$this a lmu:HostNode.
			FILTER(?value > '25.0'^^xsd:float)
			FILTER NOT EXISTS {
				?otherDeduction edr:deducedFrom ?temperature_obs;
					edr:deducedWith ex:ThresholdRule.
			}
			FILTER NOT EXISTS {
				?obsURI edr:usedForDeductionBy ex:ThresholdRule.
			}
			BIND(URI(CONCAT('http://example.com/ns#abovethresholdTemperaturededuction', STRUUID())) AS ?deduction )
			BIND(NOW() AS ?now)
		}
	""".
	ex:AboveThresholdTemperature rdfs:subClassOf <http://purl.oclc.org/NET/ssnx/ssn#Property>.
