package fr.irit.melodi.app.in.server;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import fr.irit.melodi.app.Controller;
import fr.irit.melodi.app.in.resources.AppEndpoint;

public class RESTServer extends Thread {

    private static final Logger LOGGER = LogManager.getLogger(RESTServer.class);
    private String baseUrl;
    private int port;
    private String url;
    private static HttpServer server;

    public RESTServer(String url, int port){
        this.port = port;
        this.baseUrl = url;
        this.url = url +":"+port;
    }

    public int getListeningPort(){
        return this.port;
    }

    public String getURL(){
        return this.url;
    }

    public void run() {
        LOGGER.info("Setting up REST server for sensor " + Controller.getInstance().getConfiguration().getName());
        URI baseUri = UriBuilder.fromUri(this.baseUrl).port(this.port).build();
        LOGGER.info("Configuring resources");
        ResourceConfig rc = new ResourceConfig()
                .register(AppEndpoint.class);
        LOGGER.info("Creating the HTTP server...");
        server = GrizzlyHttpServerFactory.createHttpServer(baseUri, rc);
        try {
            LOGGER.info("Starting REST server");
            System.out.println("Starting REST server");
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
