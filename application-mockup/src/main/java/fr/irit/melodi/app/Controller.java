package fr.irit.melodi.app;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.text.DateFormat;
import java.time.OffsetDateTime;

import javax.ws.rs.core.Response;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.melodi.app.configuration.Configuration;
import fr.irit.melodi.app.in.server.RESTServer;
import fr.irit.melodi.app.model.AppModel;
import fr.irit.melodi.app.out.resource.Notifier;
import fr.irit.melodi.app.util.File2String;
import fr.irit.melodi.sparql.exceptions.NotAFolderException;
import fr.irit.melodi.sparql.files.FolderManager;
import fr.irit.tools.queries.QueryEngine;

public class Controller {
	private static final Logger LOGGER = LogManager.getLogger(Controller.class);
	private static final Logger LOGFILE = LogManager.getLogger("LogFile");

	private static Controller instance;
	private Configuration configuration;
	private RESTServer server;
	private FolderManager queries;
	
	private Controller(Configuration config){
		this.configuration = config;
		this.server = new RESTServer(configuration.getUri(), configuration.getPort());
		try {
			this.queries = new FolderManager("queries");
			this.queries.loadQueries();
		} catch (NotAFolderException e) {
			e.printStackTrace();
		}

	}
	
	public static Controller getInstance(){
		// The controller is the first element to be created, 
		// therefore the instance is necessarily instanciated
		return Controller.instance;
	}
	
	public void newObservationProduced(String observation){
		for(String endpoint : AppModel.getInstance().getListeners()){
			Response r = Notifier.notifyObservers(endpoint, observation);
			if(r.getStatus() != 200){
				LOGGER.error("Notification failed : "+r.getStatusInfo());
			}
		}
	}
	
	public FolderManager getQueries(){
		return this.queries;
	}
	
	/**
	 * Subscription to the 
	 */
	public void subscribe(){
		for(String file : this.configuration.getRuleset()){
			String rules = File2String.readFile(new File(file));
			Response r = Notifier.subscribe(this.configuration.getEndpoint(), rules);
			if(r.getStatus() != HttpURLConnection.HTTP_OK){
				LOGGER.error("Subscription failed with status : "+r.getStatusInfo());
			} else {
				LOGGER.info("Rule successfully registered");
			}
		}
	}
	
	public static void log(String log){
		Model m = ModelFactory.createDefaultModel();
		StringReader sr = new StringReader(log);
		m.read(sr, "http://example.com/ns", "TTL");
		FolderManager queries = Controller.getInstance().getQueries();
		QueryEngine.updateQuery(queries.getQueries().get("insert_timestamp"), m);
		StringWriter sw = new StringWriter();
		m.write(sw, "TTL");
		LOGFILE.info(sw.toString());
	}
	
	public static void main(String[] args) {
		org.apache.logging.log4j.core.LoggerContext ctx =
			    (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
			ctx.reconfigure();
		if(args[0] == null){
			LOGGER.fatal("Usage : mvn exec:java -Dconfig=<configuration file path>");
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			Configuration config = mapper.readValue(new File(args[0]), Configuration.class);
			Controller.instance = new Controller(config);
			Controller.instance.server.run();
			Controller.instance.subscribe();
			
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Configuration getConfiguration() {
		return configuration;
	}
}
