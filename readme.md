# EDR simulator

## General description

This project aims at evaluating the EDR algorithm proposed by Nicolas SEYDOUX, Khalil DRIRA, Nathalie HERNANDEZ and Thierry MONTEIL. It spawns a set of processes communicating with each other in HTTP simulating an IoT network, and applications consuming data produced by the network.

## Creating a topology

A topology is described in a json file. Basically, it contains the different nodes, assuming a hierarchical distribution, and the different sensors. Reference topologies are available in the `simulator` folder. A predefined set of sensor types can be used (see `simulator/generator.py`). Future works will involve replaying a real dataset.

## Creating an application

Simulated applications' behaviour is dictated by production rules. Applications are described by a json file. When they start, they send their rules to the top node of the network, and log the result of their production rules they receive.

## Starting the simulation

The simulation is launched by the `initializer.py` script, with two arguments: the folder containing the topology (previously populated by the generation script), and the folder containing applications descriptions. All the events happening during the simulation are logged, either by the nodes or by the applications, in the `logs` folder.

## Getting results

The result script `simulator/results.py` computes the average time between the production of observations, and the time where these observations are used for a deduction, as well as the moment when they are received by target applications. 
