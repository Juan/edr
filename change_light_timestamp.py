import os
import glob
import argparse
import json
import csv
import dateutil.parser

def change_hosts(node, node_type, host):
    if "type" in node and node["type"]==node_type:
        node["host"]=host
    if "concentrates" in node:
        for child in node["concentrates"]:
            change_hosts(child, node_type, host)


parser = argparse.ArgumentParser(description='Formats adream sensor data to be replayed by an EDR simulation.')
parser.add_argument('-s', '--source', help="CSV to update", type=str)

args = parser.parse_args()
with open(args.source, newline='') as observations:
    with open("tmp_light_csv", "w") as target:
        reader = csv.reader(observations)
        for observation in reader:
            time = dateutil.parser.parse(observation[0])
            # 2018-02-25 03:12:00+01:00
            observation[0] = "{0}-{1}-{2} {3}:{4}:{5}+01:00".format(time.year, str(time.month).zfill(2), str(time.day).zfill(2), str(time.hour).zfill(2), str(time.minute).zfill(2), str(time.second).zfill(2))
            target.write(",".join(observation)+"\n")
os.remove(args.source)
os.rename("tmp_light_csv", args.source)
# with open(args.topology, "r") as topo_file:
#     topology = json.load(topo_file)
#     change_hosts(topology["layers"], args.nodetype, args.host)
# with open(args.topology, "w") as topo_file:
#     json.dump(topology, topo_file)
