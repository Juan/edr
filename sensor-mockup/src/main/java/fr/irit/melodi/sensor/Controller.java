package fr.irit.melodi.sensor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.irit.melodi.sensor.configuration.Configuration;
import fr.irit.melodi.sensor.in.server.RESTServer;
import fr.irit.melodi.sensor.model.Model;
import fr.irit.melodi.sensor.out.resource.Notifier;

public class Controller {
	private static final Logger LOGGER = LogManager.getLogger(Controller.class);
	
	private static Controller instance;
	private Map<String, Configuration> configurations;
	private Map<String, RESTServer> servers;
	private Map<String, Sensor> sensors;
	
	private Controller(Map<String, Configuration> config){
		this.configurations = config;
		this.servers = new HashMap<>();
		this.sensors = new HashMap<>();
		for(Entry<String, Configuration> conf : this.configurations.entrySet()){
			this.servers.put(conf.getKey(), new RESTServer(conf.getValue().getHost(), conf.getValue().getPort()));
			try {
				this.sensors.put(conf.getKey(), new Sensor(conf.getValue()));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Controller getInstance(){
		// The controller is the first element to be created, 
		// therefore the instance is necessarily instanciated
		return Controller.instance;
	}
	
	public void newObservationProduced(Integer port, String observation){
		for(String endpoint : Model.getInstance(port).getListeners()){
			Response r = Notifier.notifyObservers(endpoint, observation);
			if(r.getStatus() != 200){
				LOGGER.error("Notification failed : "+r.getStatusInfo());
			}
		}
	}
	
	public static void main(String[] args) {
		if(args[0] == null){
			LOGGER.fatal("Usage : mvn exec:java -Dconfig=<configuration files path> -Ddata=<CSV file to be replayed>");
		}
		Map<String, Configuration> configurations = new HashMap<>();
		try {
			String glob = args[0].split("/")[args[0].split("/").length-1];
			int splitIndex = args[0].indexOf(glob, 0);
			String dir = args[0].substring(0, splitIndex);
		    try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
		            Paths.get(dir), glob)) {
		        dirStream.forEach(path -> {
					try {
						System.out.println("Adding "+path.toString()+" to configurations");
						ObjectMapper mapper = new ObjectMapper();
						Configuration c = mapper.readValue(new File(path.toString()), Configuration.class);
						configurations.put(c.getName(), c);
					} catch (JsonParseException e) {
						e.printStackTrace();
					} catch (JsonMappingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		    );
		    } 
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Controller.instance = new Controller(configurations);
		for(RESTServer server : Controller.instance.servers.values()){
			server.start();
		}
		// Wait until the servers are started
		try {
			Thread.sleep(Controller.instance.sensors.values().size()*10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(Sensor sensor : Controller.instance.sensors.values()){
			sensor.start();
		}
	}
}
