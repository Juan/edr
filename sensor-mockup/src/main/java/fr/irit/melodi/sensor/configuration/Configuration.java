package fr.irit.melodi.sensor.configuration;

public class Configuration {
	private Integer id;
	// Sleep time between two observations
	private Integer cycleDuration;
	private String name;
	private Float lowerBound;
	private Float upperBound;
	private Float rate;
	private String host;
	private Integer port;
	private String measureType;
	private String measures;
	// Never used, but useful if sensors become more intelligent
	private Boolean reasoning;
	private String datafile;
	private String sensorId;
	private Integer initialSleep;

	public Integer getCycleDuration() {
		return cycleDuration;
	}

	public void setCycleDuration(Integer cycleDuration) {
		this.cycleDuration = cycleDuration;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(Float lowerBound) {
		this.lowerBound = lowerBound;
	}

	public Float getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(Float upperBound) {
		this.upperBound = upperBound;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getMeasureType() {
		return measureType;
	}

	public void setMeasureType(String measureType) {
		this.measureType = measureType;
	}

	public Float getRate() {
		return rate;
	}

	public void setRate(Float rate) {
		this.rate = rate;
	}

	public String getMeasures() {
		return measures;
	}

	public void setMeasures(String measures) {
		this.measures = measures;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getReasoning() {
		return reasoning;
	}

	public void setReasoning(Boolean reasoning) {
		this.reasoning = reasoning;
	}

	public String getDatafile() {
		return datafile;
	}

	public void setDatafile(String datafile) {
		this.datafile = datafile;
	}

	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public Integer getInitialSleep() {
		return initialSleep;
	}

	public void setInitialSleep(Integer initialSleep) {
		this.initialSleep = initialSleep;
	}
	
}
